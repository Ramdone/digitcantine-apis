<!DOCTYPE html>
<html lang="en" style="margin: 0px; padding: 0px; width: 100vw;">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Menu disponible</title>
</head>
<body style="margin: 0px; padding: 0px; width: 100%; height: 100%;">
<div style="background-color: white; display: flex; flex-direction: column;
      align-items: center; padding: 30px;">
    <div style="padding: 50px 15px; border-radius: 10px;
        background-color: #00000005; display: flex;
       text-align: center; align-items: center !important; align-self: center;">
        <table style="align-self: center">
            <tr>
                <td></td>
                <td style="text-align: center;">
                    <div style="font-size: 30px; font-weight: bold; color: #1F9D92;
                text-align: center; margin-bottom: 50px;">
                        DigiCantine
                    </div>
                </td>
                <td></td>
            </tr>
            <tr></tr>
            <tr></tr>
            <tr>
                <td></td>
                <td style="text-align: center;">
                    <div style="text-align: center;">
                        Hello chers usagers !
                    </div>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td style="text-align: center;">
                    <div style="text-align: center;">
                        Le menu du {{$data['dateDebut']}} au {{$data['dateFin']}} est prêt. Veuillez vous connecter pour faire vos réservations.
                    </div>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
</div>
</body>
</html>

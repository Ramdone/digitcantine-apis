<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


// to login
Route::post('/login', [\App\Http\Controllers\AuthController::class, 'login']);
// to send mail for password reset
Route::post('/resetPasswordMail', [\App\Http\Controllers\AuthController::class, 'resetPasswordMail']);

// to download file exemple
Route::get('/downloadExemple', [\App\Http\Controllers\UserController::class, 'download']);

// should be loged in before contact all these routes
Route::group(['middleware' => ['auth:sanctum','enabled']], function () {
    // to ping
    Route::get('/ping', function (){
        return response()->json(['success'=>true]);
    });


    // to create new users agent
    Route::post('/createAgent', [\App\Http\Controllers\AuthController::class, 'createAgent'])->middleware('role:AT,AR');
    // to create user Usager
    Route::post('/createUsager', [\App\Http\Controllers\AuthController::class, 'createUsager'])->middleware('role:AT,AF');
    // to get connected user
    Route::get('/me', [\App\Http\Controllers\AuthController::class, 'me']);
    // to reset password
    Route::post('/resetPassword', [\App\Http\Controllers\AuthController::class, 'resetPassword']);
    // to change password
    Route::post('/changePassword', [\App\Http\Controllers\AuthController::class, 'changePassword']);
    // to update username
    Route::post('/updateUser', [\App\Http\Controllers\AuthController::class, 'updateUser'])->middleware('role:AT,AF,AR');
    // to update user access cantine
    Route::post('/updateAccesCantine', [\App\Http\Controllers\UserController::class, 'updateAccesCantine'])->middleware('role:AT,AF,AR');
    // to update usager information
    Route::post('/updateUsager', [\App\Http\Controllers\UserController::class, 'updateUsager'])->middleware('role:AT,AF');
    // to upload user profil picture
    Route::post('/addUserProfil', [\App\Http\Controllers\UserController::class, 'addUserProfil'])->middleware('role:AT,AF,UG');
    // to delete a user
    Route::delete('/deleteUser/{id}', [\App\Http\Controllers\UserController::class, 'deleteUser'])->middleware('role:AT,AF,AR');
    Route::post('/userStatus', [\App\Http\Controllers\UserController::class, 'userStatus'])->middleware('role:AT,AF,AR');

    // to get neccessery datas to create user Usager
    Route::get('/getDatas', [\App\Http\Controllers\UserController::class, 'getDatas']);
    // to get list User Usager
    Route::get('/getUsagers', [\App\Http\Controllers\UserController::class, 'getUsagers'])->middleware('role:AT,AF');
    // to count usager and group by
    Route::get('/countUsager', [\App\Http\Controllers\UserController::class, 'countUsager'])->middleware('role:AT,AF');
    // to import Usagers
    Route::post('/importUsagers', [\App\Http\Controllers\UserController::class, 'importUsagers'])->middleware('role:AT,AF');
    // to export usager
    Route::get('/exportUsager', [\App\Http\Controllers\UserController::class, 'exportUsager'])->middleware('role:AT,AF');
    // autocomplete get Usager
    Route::get('/getUsagerAutocomplete', [\App\Http\Controllers\UserController::class, 'getUsagerAutocomplete']);
    // to get agent list
    Route::get('/getAgent', [\App\Http\Controllers\UserController::class, 'getAgent'])->middleware('role:AT,AR');




    // to create a Poste
    Route::post('/createPost', [\App\Http\Controllers\PosteController::class, 'createPost'])->middleware('role:AT,AF,AR');
    // to get Poste list
    Route::get('/getPost', [\App\Http\Controllers\PosteController::class, 'getPost'])->middleware('role:AT,AF,AR');
    // to delete specific Post
    Route::delete('/deletePost/{id}', [\App\Http\Controllers\PosteController::class, 'deletePost'])->middleware('role:AT,AF,AR');

    // to create a Categorie
    Route::post('/createCategorie', [\App\Http\Controllers\CategorieController::class, 'createCategorie'])->middleware('role:AT,AF,AR');
    // to get Categorie list
    Route::get('/getCategorie', [\App\Http\Controllers\CategorieController::class, 'getCategorie'])->middleware('role:AT,AF,AR');
    // to delete a Categorie
    Route::delete('/deleteCategorie/{id}', [\App\Http\Controllers\CategorieController::class, 'deleteCategorie'])->middleware('role:AT,AF,AR');

    // to create a Structure
    Route::post('/createStructure', [\App\Http\Controllers\StructureController::class, 'createStructure'])->middleware('role:AT,AF,AR');
    // to get Structure list
    Route::get('/getStructure', [\App\Http\Controllers\StructureController::class, 'getStructure'])->middleware('role:AT,AF,AR');
    // to delete a Structure
    Route::delete('/deleteStructure/{id}', [\App\Http\Controllers\StructureController::class, 'deleteStructure'])->middleware('role:AT,AF,AR');

    // to create a type
    Route::post('/createType', [\App\Http\Controllers\TypeController::class, 'createType'])->middleware('role:AT,AF,AR');
    // to get Type list
    Route::get('/getType', [\App\Http\Controllers\TypeController::class, 'getType'])->middleware('role:AT,AF,AR');
    // to delete a Type
    Route::delete('/deleteType/{id}', [\App\Http\Controllers\TypeController::class, 'deleteType'])->middleware('role:AT,AF,AR');

    // to create a type Absence
    Route::post('/createTypeAbsence', [\App\Http\Controllers\TypeAbsenceController::class, 'createTypeAbsence'])->middleware('role:AT,AF,AR');
    // to get Type list
    Route::get('/getTypeAbsence', [\App\Http\Controllers\TypeAbsenceController::class, 'getTypeAbsence'])->middleware('role:AT,AF,AR');
    // to delete a Type
    Route::delete('/deleteTypeAbsence/{id}', [\App\Http\Controllers\TypeAbsenceController::class, 'deleteTypeAbsence'])->middleware('role:AT,AF,AR');

    // to create Usager Absence
    Route::post('/createAbsence', [\App\Http\Controllers\AbsenceController::class, 'createAbsence'])->middleware('role:AT,AF');
    // to update user Absence
    Route::post('/updateAbsence', [\App\Http\Controllers\AbsenceController::class, 'updateAbsence'])->middleware('role:AT,AF');
    // to get a user absence
    Route::get('/absence/{id}', [\App\Http\Controllers\AbsenceController::class, 'getUserAbsences'])->middleware('role:AT,AF');
    // to delete a Type
    Route::delete('/deleteAbsence/{id}', [\App\Http\Controllers\AbsenceController::class, 'delete'])->middleware('role:AT,AF');


    // to upload food image into galerie
    Route::post('/addImage', [\App\Http\Controllers\GalerieController::class, 'addImage'])->middleware('role:AT,AF,AR');
    // to get all galerie image
    Route::get('/getGalerie', [\App\Http\Controllers\GalerieController::class, 'getGalerie'])->middleware('role:AT,AF,AR');
    // to delete images from galerie
    Route::post('/deleteImages', [\App\Http\Controllers\GalerieController::class, 'deleteImages'])->middleware('role:AT,AF,AR');



    // to create a TypeRepas
    Route::post('/createTypeRepas', [\App\Http\Controllers\RepasMenuController::class, 'createTypeRepas'])->middleware('role:AT,AF,AR');
    // to get Type list
    Route::get('/getTypeRepas', [\App\Http\Controllers\RepasMenuController::class, 'getTypeRepas'])->middleware('role:AT,AF,AR');
    // to delete a Type
    Route::delete('/deleteTypeRepas/{id}', [\App\Http\Controllers\RepasMenuController::class, 'deleteTypeRepas'])->middleware('role:AT,AF,AR');

    // to create a TypeRepas
    Route::post('/createService', [\App\Http\Controllers\ServiceController::class, 'createService'])->middleware('role:AT,AF,AR');
    // to get Type list
    Route::get('/getService', [\App\Http\Controllers\ServiceController::class, 'getService'])->middleware('role:AT,AF,AR');
    // to delete a Type
    Route::delete('/deleteService/{id}', [\App\Http\Controllers\ServiceController::class, 'deleteService'])->middleware('role:AT,AF,AR');


    // to create a TypeMenu
    Route::post('/createTypeMenu', [\App\Http\Controllers\TypeMenuController::class, 'createTypeMenu'])->middleware('role:AT,AF,AR');
    // to get Type menu list
    Route::get('/getTypeMenu', [\App\Http\Controllers\TypeMenuController::class, 'getTypeMenu'])->middleware('role:AT,AF,AR');
    // to delete a Type menu
    Route::delete('/deleteTypeMenu/{id}', [\App\Http\Controllers\TypeMenuController::class, 'deleteTypeMenu'])->middleware('role:AT,AF,AR');

    // to create  ElementRepas
    Route::post('/createElementRepas', [\App\Http\Controllers\RepasMenuController::class, 'createElementRepas'])->middleware('role:AT,AF,AR');
    // to get ElementRepas list
    Route::get('/getElementRepas', [\App\Http\Controllers\RepasMenuController::class, 'getElementRepas'])->middleware('role:AT,AF,AR');
    // to delete ElementRepas
    Route::delete('/deleteElementRepas/{id}', [\App\Http\Controllers\RepasMenuController::class, 'deleteElementRepas'])->middleware('role:AT,AF,AR');

    // to create Repas
    Route::post('/createRepas', [\App\Http\Controllers\RepasMenuController::class, 'createRepas'])->middleware('role:AT,AR');
    // to get repas with autocomplete
    Route::get('/repasAuto', [\App\Http\Controllers\RepasMenuController::class, 'repasAuto'])->middleware('role:AT,AR');

    // to create Menu
    Route::post('/createMenu', [\App\Http\Controllers\RepasMenuController::class, 'createMenu'])->middleware('role:AT,AR');
    // to edit menu
    Route::post('/editMenu', [\App\Http\Controllers\RepasMenuController::class, 'editMenu'])->middleware('role:AT,AR');
    //check if menu has reservation before delete
    Route::post('/checkMenu', [\App\Http\Controllers\RepasMenuController::class, 'checkMenu'])->middleware('role:AT,AR');
    // to delete menu
    Route::delete('/deleteMenu', [\App\Http\Controllers\RepasMenuController::class, 'deleteMenu'])->middleware('role:AT,AR');

    // to get Menu of Day
    Route::get('/getMenuOfDay', [\App\Http\Controllers\RepasMenuController::class, 'getMenuOfDay']);
    // to get Menu of week
    Route::get('/getMenuOfWeek', [\App\Http\Controllers\RepasMenuController::class, 'getMenuOfWeek']);
    // to send menu ready notif to usagers
    Route::post('/sendMenuMailNotif', [\App\Http\Controllers\RepasMenuController::class, 'sendMenuMailNotif'])->middleware('role:AT,AR');

    // to create  ElementRepas
    Route::post('/createComptoir', [\App\Http\Controllers\ComptoirController::class, 'createComptoir'])->middleware('role:AT,AF,AR');
    // to get ElementRepas list
    Route::get('/getComptoir', [\App\Http\Controllers\ComptoirController::class, 'getComptoir']);
    // to delete ElementRepas
    Route::delete('/deleteComptoir/{id}', [\App\Http\Controllers\ComptoirController::class, 'deleteComptoir'])->middleware('role:AT,AF,AR');
    // to open comptoir
    Route::post('/openComptoir', [\App\Http\Controllers\ComptoirController::class, 'openComptoir'])->middleware('role:AT,AR');
    // to get list of comptoir program
    Route::get('/getComptoirProgram', [\App\Http\Controllers\ComptoirController::class, 'getComptoirProgram'])->middleware('role:AT,AR');
    // to update comptoir program
    Route::post('/updateComptoirProgram', [\App\Http\Controllers\ComptoirController::class, 'updateComptoirProgram'])->middleware('role:AT,AR');
    // to delete comptoir program
    Route::delete('/deleteComptoirProgram/{id}', [\App\Http\Controllers\ComptoirController::class, 'deleteComptoirProgram'])->middleware('role:AT,AR');
    // to close comptoir
    Route::post('/closeComptoir', [\App\Http\Controllers\ComptoirController::class, 'closeComptoir'])->middleware('role:AT,AR');
    // to get cantine status and comptir if open
    Route::get('/getCantineStatus', [\App\Http\Controllers\ComptoirController::class, 'getCantineStatus'])->middleware('role:AT,AR,RS');


    // to get pending access control
    Route::get('/getPendingAccessControl', [\App\Http\Controllers\AccessControlController::class, 'getPendingAccessControl'])->middleware('role:AT,AF,AR');
    // to get current acces control
    Route::get('/getCurrentAccessControl', [\App\Http\Controllers\AccessControlController::class, 'getCurrentAccessControl'])->middleware('role:AT,AF,AR');
    // to ask for cancelling controll access
    Route::post('/askCancelControl', [\App\Http\Controllers\AccessControlController::class, 'askCancelControl'])->middleware('role:AT,AR,AF');

    // to cancel control access
    Route::post('/actionOnAskCancel', [\App\Http\Controllers\AccessControlController::class, 'actionOnAskCancel'])->middleware('role:AT,AF');
    // to get control access list
    Route::get('/getAccesContols', [\App\Http\Controllers\AccessControlController::class, 'getAccesContols'])->middleware('role:AT,AF,AR');
    // to allow access control
    Route::post('/allowAccess', [\App\Http\Controllers\AccessControlController::class, 'allowAccess'])->middleware('role:AT');



    // to make reservation
    Route::post('/makeReservation', [\App\Http\Controllers\ReservationController::class, 'makeReservation']);
    // to get user revervations
    Route::get('/getMyReservation', [\App\Http\Controllers\ReservationController::class, 'getMyReservation']);
    // to get list of reservation of all usager
    Route::get('/getReservations', [\App\Http\Controllers\ReservationController::class, 'getReservations']);
    // to delete My reservation
    Route::delete('/deleteReservation/{id}', [\App\Http\Controllers\ReservationController::class, 'deleteReservation']);


    // to badge a comptoir
    Route::post('/doBadge', [\App\Http\Controllers\ConsommationController::class, 'performBadge'])->middleware('role:AT,RS,AR');
    // to get list of bagde for current comptoir
    Route::get('/getBadgeList/{myComptoir_id}', [\App\Http\Controllers\ConsommationController::class, 'getBadgeList'])->middleware('role:AT,RS');
    // to delete badge account
    Route::delete('/deletebadge/{id}', [\App\Http\Controllers\ConsommationController::class, 'deletebadge'])->middleware('role:AT,AF,AR,RS');
    // get list of consommation
    Route::get('/getConsommationlist', [\App\Http\Controllers\ConsommationController::class, 'getConsommationlist'])->middleware('role:AT,AF,AR');
    // get list of connected user consommation
    Route::get('/getMyConsommationList', [\App\Http\Controllers\ConsommationController::class, 'getConsommationlist']);

    // to set settings
    Route::post('/storeSettings', [\App\Http\Controllers\ParametreController::class, 'storeSettings'])->middleware('role:AT');

    // to get logs of modifications on user table
    Route::get('/getLogs', [\App\Http\Controllers\LogController::class, 'getLogs'])->middleware('role:AT,AF');

    // to get etat consommation by periode
    Route::get('/getConsommationByPeriode', [\App\Http\Controllers\EtatController::class, 'getConsommationByPeriode'])->middleware('role:AT,AF');
    // to get consommation by comptoir
    Route::get('/getConsommationByCompotoir', [\App\Http\Controllers\EtatController::class, 'getConsommationByCompotoir'])->middleware('role:AT,AF');
    // to get consommation by month
    Route::get('/getConsommationByMonth', [\App\Http\Controllers\EtatController::class, 'getConsommationByMonth'])->middleware('role:AT,AF');

    Route::get('/getConsommationByYear', [\App\Http\Controllers\EtatController::class, 'getConsommationByYear'])->middleware('role:AT,AF');













});

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->boolean('default_password_change')->default(false);
            $table->string('profil');
            $table->string('codeRole')->nullable();
            $table->string('photo')->nullable();
            $table->string('matricule')->nullable();
            $table->string('codeBadge')->nullable();
            $table->boolean('status')->default(true);
            $table->boolean('access_cantine')->default(true);
            $table->text('motif_access_denied')->nullable();
            $table->integer('nbr_repas')->default(1);
            $table->integer('created_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->string('deleted_at')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('poste_id')->nullable();
            $table->integer('type_id')->nullable();
            $table->integer('structure_id')->nullable();
            $table->integer('categorie_id')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

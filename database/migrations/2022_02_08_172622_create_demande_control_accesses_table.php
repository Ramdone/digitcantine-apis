<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDemandeControlAccessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('demande_control_access', function (Blueprint $table) {
            $table->id();
            $table->string('motif')->nullable();
            $table->integer('status')->default(0); // 0= en attente, 1 = valider, 2 = rejeter
            $table->string('motifRejet')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('validated_by')->nullable();
            $table->string('validated_at')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('demande_control_accesses');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTableLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_table_logs', function (Blueprint $table) {
            $table->id();
            $table->integer('created_by')->nullable();
            $table->integer('user_id')->nullable();
            $table->longText('object_before')->nullable();
            $table->longText('object_after')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_table_logs');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOuvertureComptoirsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ouverture_comptoirs', function (Blueprint $table) {
            $table->id();
            $table->integer('menuRepas_id');
            $table->integer('user_aff_id');
            $table->integer('comptoir_id');
            $table->dateTime('dateDebut');
            $table->dateTime('dateFin');
            $table->integer('created_by');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ouverture_cpontoirs');
    }
}

<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class RoleTableSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas=
            array(
                [
                    'id'=>1,
                    'libelle' => 'Admin Fonctionnel',
                    'code'=>'AF',
                ],
                [
                    'id'=>2,
                    'libelle' => 'Admin Restaurateur',
                    'code'=>'AR',
                ],
                [
                    'id'=>3,
                    'libelle' => 'Restaurateur Simple',
                    'code'=>'RS',
                ],
                [
                    'id'=>4,
                    'libelle' => 'Admin Technique',
                    'code'=>'AT',
                ],
                [
                    'id'=>5,
                    'libelle' => 'Usager',
                    'code'=>'UG',
                ],

            );

        DB::table('roles')->delete();
        if (env('DB_CONNECTION') == 'sqlsrv'){
            DB::unprepared('SET IDENTITY_INSERT roles ON');
        }
        foreach ($datas as $cat)
        {
            if (!Role::where('code',$cat['code'])->exists()){
                Role::create($cat);
            }
        }

        if (env('DB_CONNECTION') == 'sqlsrv'){
            DB::unprepared('SET IDENTITY_INSERT roles OFF');
        }
    }



}

<?php

namespace Database\Seeders;

use App\Models\User;
use App\Tools\UserTools;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserTableSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                'name' => 'ADMIN Technique',
                'email' => 'hello+at@isis-ai.com',
                'password' => Hash::make('passepasse'),
                'profil' => UserTools::AGENTPROFIL,
                'codeRole' => 'AT',
            ],
            [
                'name' => 'ADMIN Fonctinnel',
                'email' => 'hello+af@isis-ai.com',
                'password' => Hash::make('passepasse'),
                'profil' => UserTools::AGENTPROFIL,
                'codeRole' => 'AF',
            ],
            [
                'name' => 'Admin Restaurateur',
                'email' => 'hello+ar@isis-ai.com',
                'password' => Hash::make('passepasse'),
                'profil' => UserTools::AGENTPROFIL,
                'codeRole' => 'AR',
            ],
            [
                'name' => 'Restaurateur Simple',
                'email' => 'hello+rs@isis-ai.com',
                'password' => Hash::make('passepasse'),
                'profil' => UserTools::AGENTPROFIL,
                'codeRole' => 'RS',
            ]
        );

      //  DB::table('users')->delete();

        foreach ($data as $item){
            if (!User::where('email',$item['email'])->exists()){
                User::create($item);
            }
        }

    }
}

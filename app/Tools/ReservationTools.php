<?php


namespace App\Tools;


use App\Exports\ConsommationExport;
use App\Exports\ReservationExport;
use App\Models\MenuRepas;
use App\Models\Reservation;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ReservationTools
{
    // tools to make reservation
    public static function reservation($request){
        try {
            $menurepas = MenuRepas::find($request->menuRepas_id);
            if ($menurepas->date < Carbon::now()->startOfDay()){
                return ApiResponseFormatTools::Format(false,'Reservation impossible pour une date antérieur');
            }
          //  if (! Reservation::where(['created_by' => Auth::user()->id, 'dateReservedFor'=> $menurepas->date])->first()){
            if (! Reservation::where('created_by', Auth::user()->id)->whereDate('dateReservedFor', $menurepas->date)->first()){
                $request['created_by'] = Auth::user()->id;
                $request['dateReservedFor'] = Carbon::parse($menurepas->date)->format(UtilsTools::projectDateFormat());
                $reservation =  Reservation::create($request->all());
                return ApiResponseFormatTools::Format(true,'',$reservation);
            }

            return ApiResponseFormatTools::Format(false,'Vous avez déja fait une  reservation pour cette date ');

        } catch (\Exception $e) {
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);
        }
    }

    // to get connected user reservations
    public static function myReservation($request){
        try {
            $limit = UtilsTools::limit($request);
            $page = UtilsTools::page($request);
            $skip = UtilsTools::skip($page,$limit);

            $dateStart = isset($request->dateDebut) ? Carbon::parse($request->dateDebut)->startOfDay()->format(UtilsTools::projectDateFormat()) : Carbon::now()->startOfDay()->format(UtilsTools::projectDateFormat());
            $dateEnd = isset($request->dateFin) ? Carbon::parse($request->dateFin)->endOfDay()->format(UtilsTools::projectDateFormat()) : Carbon::now()->endOfDay()->format(UtilsTools::projectDateFormat());

            $reservations = Reservation::where('created_by', Auth::user()->id)
                ->where('dateReservedFor','>=', $dateStart);
            if (isset($request->dateFin)){
                $reservations = $reservations->where('dateReservedFor','<=',$dateEnd);
            }
            $reservations = $reservations->orderby('dateReservedFor')->with(['menuRepas']);

            $all = $reservations->count();
            $lastpage = UtilsTools::lastPage($all,$limit);

            $reservations = $reservations->limit($limit)->skip($skip)->get();

            return ApiResponseFormatTools::FormatPaginate(true,'',$all,$limit,$page, $lastpage, $reservations);

        }catch (\Exception $e) {
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);
        }
    }

    // to get list of reservations
    public static function listReservation($request){
        try {
            $limit = UtilsTools::limit($request);
            $page = UtilsTools::page($request);
            $skip = UtilsTools::skip($page,$limit);

            $dateStart = isset($request->dateDebut) ? Carbon::parse($request->dateDebut)->startOfDay()->format(UtilsTools::projectDateFormat()) : Carbon::now()->startOfDay()->format(UtilsTools::projectDateFormat());
            $dateEnd = isset($request->dateFin) ? Carbon::parse($request->dateFin)->endOfDay()->format(UtilsTools::projectDateFormat()) : Carbon::now()->endOfDay()->format(UtilsTools::projectDateFormat());

            $reservations = Reservation::whereBetween('dateReservedFor', [$dateStart,$dateEnd])
                ->whereHas('usager', function ($query) use($request) {
                    if (isset($request->name)){
                        $query->where('name', 'LIKE', "%$request->name%");
                    }

                    if (isset($request->access_cantine)){
                        $query->where('access_cantine',$request->access_cantine);
                    }

                });
            if (isset($request->repas_id)){
                $reservations = $reservations->whereHas('menuRepas', function ($menuRepas) use($request){
                    $menuRepas->whereRelation('repas','id',$request->repas_id);
                });
            }

            $reservations = $reservations->with(['usager','menuRepas']);

            if (isset($request->export) && $request->export=='EXCEL' ){
                return Excel::download(new ReservationExport($reservations->get()), 'reservation.xlsx');
            }

            $all = $reservations->count();
            $lastpage = UtilsTools::lastPage($all,$limit);
            $reservations = $reservations->limit($limit)->skip($skip)->get();
            $stat = self::statReservationByPeriode($dateStart,$dateEnd);
            $data = ['reservations' =>$reservations, 'stats' => $stat];
            return ApiResponseFormatTools::FormatPaginate(true,'',$all,$limit,$page, $lastpage, $data);

        }catch (\Exception $e) {
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);
        }
    }


    // to get stat of reservation in a periode
    public static function statReservationByPeriode($dateStart,$dateEnd){
        $sql ="select repas.id, repas.libelle, count(reservations.id) as nbr
               From repas, menu_repas, reservations
               where repas.id=menu_repas.repas_id
               and menu_repas.id=reservations.menuRepas_id
               and reservations.dateReservedFor between '".$dateStart."' and '".$dateEnd."'
               /*group By(repas.id)*/
               group By repas.id, repas.libelle
               ";

        return DB::select(DB::raw($sql));
    }

}

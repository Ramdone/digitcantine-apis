<?php


namespace App\Tools;


use App\Models\Categorie;
use App\Models\Consommation;
use App\Models\Parametre;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use function Doctrine\Common\Cache\Psr6\get;

class EtatTools
{

    // etat consommation by categorie and or by categorie
    public static function consommationByPeriode($request){
        try {
            $dateStart = isset($request->dateDebut) ? Carbon::parse($request->dateDebut)->startOfDay() : Carbon::now()->startOfDay();
            $dateEnd = isset($request->dateFin) ? Carbon::parse($request->dateFin)->endOfDay() : Carbon::now()->endOfDay();

            $table = self::getDonnee($request, $dateStart, $dateEnd);
            return ApiResponseFormatTools::Format(true,'',$table);

        } catch (\Exception $e) {
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);
        }
    }


    // etat consommation by month
    public static function consommationByMonth($request){
        try {
            $month = isset($request->month) ? $request->month : Carbon::now()->month;
            $year = isset($request->year) ? $request->year :Carbon::now()->year;

            $date = $year.'-'.$month.'-01';

            $dateStart = Carbon::parse($date)->startOfMonth();
            $dateEnd = Carbon::parse($date)->endOfMonth();

            $table = self::getDonnee($request, $dateStart, $dateEnd);
            return ApiResponseFormatTools::Format(true,'',$table);

        } catch (\Exception $e) {
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);
        }
    }



    private static function getDonnee($request, $dateStart, $dateEnd ){

        $parametre = Parametre::firstOrCreate(['id'=>1]);
        $period = CarbonPeriod::create($dateStart, $dateEnd);

        $all = [];
        $total = null;
        foreach ($period as $date){
            $dayFrom = $date->startOfDay()->format(UtilsTools::projectDateFormat());
            $dayEnd = $date->endOfDay()->format(UtilsTools::projectDateFormat());
            $consommation = Consommation::whereBetween('created_at', [$dayFrom, $dayEnd]);
            if (isset($request->categorie_id)){
                $consommation = $consommation->whereHas('usager', function ($query) use($request) {
                    $query->where('categorie_id',$request->categorie_id);
                    //    $query->whereRelation('categorie','id',$request->categorie_id);
                });
            }

            $consommation = $consommation->count();
            $first = Consommation::whereBetween('created_at', [$dayFrom, $dayEnd])->first();
            $montant = isset($first->montant_repas) ? (int)$first->montant_repas : (int)$parametre->montant_repas;
            $data = [
                "date" => $date->format('d/m/Y'),
                "qty" => $consommation,
                "price" => $montant,
                "montant" => $consommation * $montant,
            ];
            $all[] = $data;
            $total = $total + $data['montant'];
        }
        $table = ['table' => $all, 'total'=>$total];

        if (isset($request->categorie_id)){
            $table['categorie'] = Categorie::find($request->categorie_id);
        }

        return $table;
    }


    // consummation by comptoir
    public static function consommationByCompotoir($request){
        try {
            $dateStart = isset($request->dateDebut) ? Carbon::parse($request->dateDebut)->startOfDay()->format(UtilsTools::projectDateFormat()) : Carbon::now()->startOfDay()->format(UtilsTools::projectDateFormat());
            $dateEnd = isset($request->dateFin) ? Carbon::parse($request->dateFin)->endOfDay()->format(UtilsTools::projectDateFormat()) : Carbon::now()->endOfDay()->format(UtilsTools::projectDateFormat());
            $parametre = Parametre::firstOrCreate(['id'=>1]);

            $consommation = Consommation::whereBetween('created_at',[$dateStart,$dateEnd])->get()
                ->groupby(function ($item, $key){
                    return date('d/m/Y',strtotime($item->created_at));
                })->map(function ($item) use ($parametre){
                    $item = $item->groupby(function ($item, $key){
                        return date('H:i', strtotime($item->ouvertureComptoir->dateDebut))." - ". date('H:i', strtotime($item->ouvertureComptoir->dateFin));

                    })->map(function ($item) use ($parametre){
                        $montant = isset($item[0]->montant_repas) ? $item[0]->montant_repas : $parametre->montant_repas;
                        $total = count($item);
                        $montantTotal = $montant * $total;
                        return [
                            'qty' => $total,
                            'price' => $montant,
                            'montant' => $montantTotal
                        ];
                    });
                    return $item;
                });

            $data = [];
            foreach ($consommation as $date => $item){
                $totalDay = 0;
                foreach ($item as $hours => $value) {
                    $totalDay += $value['montant'];
                }
                $data[$date]['hours'] = $item;
                $data[$date]['totalDay'] = $totalDay;
            }

            return ApiResponseFormatTools::Format(true,'',$data);

        } catch (\Exception $e) {
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);
        }


    }



    // consummation by year
    public static function consommationByYear($request){
        try {
            $year = isset($request->year) ? $request->year :Carbon::now()->year;
            $parametre = Parametre::firstOrCreate(['id'=>1]);

            $consommation = Consommation::whereYear('created_at',$year)->get()
                ->groupby(function ($item, $key){
                    return date('m',strtotime($item->created_at));
                })->map(function ($item) use ($parametre){
                    $montant = isset($item[0]->montant_repas) ? $item[0]->montant_repas : $parametre->montant_repas;
                    $total = count($item);
                    $montantTotal = $montant * $total;
                    return [
                        'qty' => $total,
                        'price' => $montant,
                        'montant' => $montantTotal
                    ];
                });
            $total = 0;
            $data = [];

            foreach ($consommation as $month => $item){
                $total += $item['montant'];
                $item['month'] = UtilsTools::getMonthLabel($month);
                $data[] = $item;
            }
            $table = ['table' => $data, 'total'=>$total];

            return ApiResponseFormatTools::Format(true,'',$table);

        } catch (\Exception $e) {
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);
        }

    }

}

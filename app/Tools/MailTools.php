<?php


namespace App\Tools;


use App\Mail\ConnectionReference;
use App\Mail\PasswordReset;
use Illuminate\Support\Facades\Mail;

class MailTools
{
    // tools to send email for password reset
    public static function sendResetPasswordMail($request, $user){
        try {
            $data = array(
                'name' => $user->name,
                'url' => $request->url.'?access_token='.$user->createToken('auth_token')->plainTextToken,
            );
            Mail::to($user->email)->send(new PasswordReset($data));
            return ApiResponseFormatTools::Format(true,'');
        }catch (\Exception $e){
            return ApiResponseFormatTools::Format(false, $e->getMessage());
        }
    }


    // to send connection informations to new user created
    public static function sendConnectionreferenceMail($request, $user,$password){

        $data = array(
            'name' => $user->name,
            'email' => $user->email,
            'password' => $password,
            'url' => $request->url.'?fristConnect=1',
        );
        Mail::to($user->email)->send(new ConnectionReference($data));

    }



}

<?php


namespace App\Tools;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CrudTools
{
    // to store data in model
    public static function store($model, $request){
        try {
            // verifier si le libelle exist
            $checkExist = $model::where(['libelle'=>$request->libelle])->first();
            if (isset($checkExist)){
                if ($checkExist->status == false){
                    $checkExist->update(array('status'=>true));
                    return ApiResponseFormatTools::Format(true,'',$checkExist);
                }else{
                    return ApiResponseFormatTools::Format(false,'Ce libellé existe déja');
                }
            }
            //      création du la donnée
            $request['created_by'] = Auth::user()->id;
            $item = $model::create($request->all());
            return ApiResponseFormatTools::Format(true,'',$item);
        } catch (\Exception $e) {
            return ApiResponseFormatTools::Format(false,$e->getMessage());
        }
    }


    // to get all valide datas from model
    public static function get($model,$request){
        $liste =  $model::where(['status' => true]);
        if (isset($request->search)){
            $liste = $liste->where('libelle', 'LIKE', "%$request->search%");
        }
        $liste = $liste->orderBy('libelle')->get();
        return $liste;

    }


    // to update datas in model
    public static function update($model, $request, $id){
        try {
            $item = $model::find($id);
            $request['updated_by'] =Auth::user()->id;
            $item->update($request->all());
            return ApiResponseFormatTools::Format(true,'Item updated',$item);

        }catch (\Exception $e){
            return ApiResponseFormatTools::Format(false,$e->getMessage());
        }
    }


    // to delete specific data from model, we just update the status
    public static function delete($model,$request, $id){
        try {
            $item = $model::find($id);
            if (isset($item)){
                $request['deleted_by'] = Auth::user()->id;
                $request['deleted_at'] = Carbon::now();
                $request['status'] = false;
                $item->update($request->all());
                return ApiResponseFormatTools::Format(true,'Item deleted');
            }
            return ApiResponseFormatTools::Format(false,'Element introuvable');
        } catch (\Exception $e) {
            return ApiResponseFormatTools::Format(false,$e->getMessage());
        }
    }


    public static function autocomplete($model,$request,$limit=100){
        try {
            $usager = $model::select('id','libelle')
                ->where('status', true)
                ->where('libelle', 'LIKE', "%$request->search%")
                ->orderby('libelle')->limit($limit)->get()
            ;
            return ApiResponseFormatTools::Format(true,'',$usager);

        }catch (\Exception $e){
            return ApiResponseFormatTools::Format(false,$e->getMessage());
        }

    }

}

<?php


namespace App\Tools;
use App\Imports\UsagersImport;
use App\Models\Categorie;
use App\Models\Poste;
use App\Models\Service;
use App\Models\Structure;
use App\Models\Type;
use App\Models\TypeMenu;
use App\Models\UserTypeMenu;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Maatwebsite\Excel\Facades\Excel;

use phpDocumentor\Reflection\Types\This;

class UserTools
{
    CONST USAGERPROFIL = 'USAGER';
    CONST AGENTPROFIL = 'AGENT';

    // tools to create agent user
    public static function createAgent(Request $request){
        DB::beginTransaction();
        try {
            $password = self::generatePassword();
            $request['profil'] =  self::AGENTPROFIL;
            $request['created_by'] = Auth::user()->id;
            $request['password'] =  Hash::make($password);

            $user = User::create($request->all());
            self::createUserTypeMenu($user->id, $request->typemenu_id);
            MailTools::sendConnectionreferenceMail($request,$user,$password);
            DB::commit();
            return ApiResponseFormatTools::Format(true,"",$user);

        }catch (\Exception $e){
            DB::rollBack();
            return ApiResponseFormatTools::Format(false,$e->getMessage());
        }

    }

    // tools to create usager user
    public static function createUsager(Request $request){
        DB::beginTransaction();
        try {
            $password = self::generatePassword();
          //  $request['password'] =  Hash::make('usagercantine2022');
            $request['password'] =  Hash::make($password);
            $request['profil'] =  self::USAGERPROFIL;
            $request['created_by'] = Auth::user()->id;

            $user = User::create($request->all());
            self::createUserTypeMenu( $user->id, $request->typemenu_id);
            MailTools::sendConnectionreferenceMail($request,$user,$password);
            DB::commit();
            return ApiResponseFormatTools::Format(true,"",$user);
        }catch (\Exception $e){
            DB::rollBack();
            return ApiResponseFormatTools::Format(false,$e->getMessage());
        }

    }


    // to import usager
    public static function importUsager($request,$data){
        $password = self::generatePassword();
        $data['password'] =  Hash::make($password);
        $data['name'] =  $data['nom'];
        $data['profil'] =  self::USAGERPROFIL;
        $data['created_by'] = Auth::user()->id;
        $data['codeRole'] = 'UG';
        $data['codeBadge'] = $data['badge'];
        $data['poste_id'] = isset($data['poste']) ? Poste::where('libelle',$data['poste'])->first()->id : null;
        $data['type_id'] = isset($data['type']) ? Type::where('libelle',$data['type'])->first()->id : null;
        $data['structure_id'] = isset($data['structure']) ? Structure::where('libelle',$data['structure'])->first()->id : null;
        $data['categorie_id'] = isset($data['categorie']) ? Categorie::where('libelle',$data['categorie'])->first()->id : null;
        $data['service_id'] = isset($data['service']) ? Service::where('libelle',$data['service'])->first()->id : null;

        $user = User::create($data);
        if(isset($data['typemenu_id'])) {
            self::createUserTypeMenu($user->id, $data['typemenu_id'],true);
        }

        MailTools::sendConnectionreferenceMail($request,$user,$password);

    }



    // to get user with all informations
    public static function getUser($id){
        $user = User::where('id',$id)
            ->with(['categorie','poste','type','structure','role','usertypemenu','service'])
            ->first();

        return $user;
    }



    // add user profil picture
    public static function userProfil($request){
        try {
            $directoryPath=public_path('profiles');
            //check if the directory exists
            if(!File::isDirectory($directoryPath)){
                //make the directory because it doesn't exists
                File::makeDirectory($directoryPath);
            }
            $fileName = time().'.'.$request->photo->extension();
            $request->photo->move($directoryPath, $fileName);

            switch ($request->type){
                case 'UPDATE':
                    $user = Auth::user();
                    // delete old profile
                    if (isset($user->photo)){
                        File::delete(public_path($user->photo));
                    }

                    $user->update(['photo' => 'profiles/'.$fileName]);
                    break;
                default :
                    break;
            }
            return ApiResponseFormatTools::Format(true,"",'profiles/'.$fileName);

        }catch (\Exception $e){
            return ApiResponseFormatTools::Format(false,$e->getMessage());
        }

    }

    // to update usager
    public static function updateUsager($request){
        try {
            $usager = User::find($request->usager_id);
            $request['updated_by'] = Auth::user()->id;

            $log_id = LogTools::createLog($usager);
            $usager->update($request->all());
            if (isset($request->typemenu_id)){
                if (isset($usager->usertypemenu)){
                    $usager->usertypemenu()->delete();
                }
                self::createUserTypeMenu($usager->id, $request->typemenu_id);
            }
            LogTools::updateLog($log_id, $usager);

            return ApiResponseFormatTools::Format(true,'',User::find($request->usager_id));

        }catch (\Exception $e){
            return ApiResponseFormatTools::Format(false,$e->getMessage());
        }
    }


    // To User type Menu
    public static function createUserTypeMenu($user_id,$data=array(),$import=false){
        foreach ($data as $item){
            UserTypeMenu::create([
                'user_id' => $user_id,
                'type_menu_id' => isset($import) && $import==true ? TypeMenu::where('libelle',$item)->first()->id : $item
            ]);
        }

    }


    // to get connected user available type menu
    public static function getUserTypeMenuIds($iduser=null){
        $id = isset($iduser) ? $iduser : Auth::user()->id;
        return UserTypeMenu::where('user_id', $id)
            ->pluck('type_menu_id')->toArray();
    }


   // to generate unique matricule
    private static function generateMatricule(){
        $number = Carbon::now()->timestamp;
        return substr($number, -5);
    }


    // to generate random password
    public static function generatePassword($length = 8){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


}

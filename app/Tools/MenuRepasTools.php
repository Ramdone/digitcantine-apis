<?php


namespace App\Tools;
use App\Exports\MenuOfWeekExport;
use App\Exports\ReservationExport;
use App\Mail\SendNotifMenuOfWeek;
use App\Models\ComposerRepas;
use App\Models\Menu;
use App\Models\MenuRepas;
use App\Models\Repas;
use App\Models\Reservation;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;


class MenuRepasTools
{
    // to create repas
    public static function createRepas($request){
        try {
            $request['created_by'] = Auth::user()->id;
            $repas = Repas::create($request->all());

            foreach ($request->elementRepas_id as $data){
                ComposerRepas::create(
                    array(
                        "repas_id" => $repas->id,
                        "elementRepas_id" => $data,
                    )
                );
            }

            return ApiResponseFormatTools::Format(true,'',$repas);

        }catch (\Exception $e){
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);

        }
    }

    // to create a Menu
    public static function createMenu($request){
        try {
            $date = new \DateTime($request->date);
            $week = intval($date->format("W")) ;

            if (env('DB_CONNECTION') == 'sqlsrv'){
                DB::unprepared('SET IDENTITY_INSERT menus ON');
            }
            $menu = Menu::firstOrCreate(['id' => $week]);

            if (env('DB_CONNECTION') == 'sqlsrv'){
                DB::unprepared('SET IDENTITY_INSERT menus OFF');
            }

            foreach ($request->repas as $data){
                MenuRepas::firstOrCreate(
                    [
                        "menu_id" => $menu->id,
                        "repas_id" => $data['repas_id'],
                        "date" => Carbon::parse($request->date)->format(UtilsTools::projectDateFormat()),
                        "type_menu_id" => $data['type_menu_id']
                    ],
                    ['reserv_is_consom' => $data['reserv_is_consom'] ]
                );
            }

            return ApiResponseFormatTools::Format(true,'');

        }catch (\Exception $e){
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);

        }
    }


    // to edit menu
    public static function editMenu($request){
        try {
            foreach ($request->datas as $data){
                $menuRepas = MenuRepas::find($data['menuRepas_id']);
                $menuRepas->update($data);
            }
            return ApiResponseFormatTools::Format(true,'');

        }catch (\Exception $e){
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);

        }

    }


    public static function checkMenu($request){
        try {
            $ids= [];
            foreach ($request->menuRepas_id as $data){
                $menuRepas = MenuRepas::find($data);
                $reservations = self::getSpecificMenuReservation($menuRepas->id);
                if ($reservations->count()> 0){
                    $ids[]=$menuRepas->id;
                }
            }
            return ApiResponseFormatTools::Format(true,'',$ids);

        }catch (\Exception $e){
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);
        }
    }

    // to get reservation for a menu
    public static function getSpecificMenuReservation($meniId){
        $reservations = Reservation::where('menuRepas_id',$meniId)->get();
        return $reservations;
    }

    // to delete menu
    public static function deleteMenu ($request){
        try {
            foreach ($request->menuRepas_id as $data){
                $menuRepas = MenuRepas::find($data);
                $reservations = self::getSpecificMenuReservation($menuRepas->id);
                if ($reservations->count()> 0){
                    foreach ($reservations as $reservation){
                        $reservation->delete();
                    }
                }
                $menuRepas->delete();
            }
            return ApiResponseFormatTools::Format(true,'');

        }catch (\Exception $e){
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);
        }



    }



    // to get a specific day Menu
    public static function menuOfDay($request){
        try {
            $date = Carbon::now();
            if (isset($request->date)){
                $date = Carbon::parse($request->date);
            }

            $menu = MenuRepas::whereDate('date', $date);
            if (isset($request->strict) && $request->strict == 1 ){
                $menu = $menu->whereIn('type_menu_id', UserTools::getUserTypeMenuIds()) ;
            }
            $menu = $menu->get();
            return ApiResponseFormatTools::Format(true,'',$menu);

        }catch (\Exception $e){
            return ApiResponseFormatTools::Format(false,$e->getMessage());

        }

    }


    // to get a specific week Menu
    public static function menuOfWeek($request){
        try {
            $week = date('W');
            if (isset($request->date)){
                $date = new \DateTime($request->date);
                $week = intval($date->format("W")) ;
            }

            $menu = MenuRepas::where('menu_id', $week);
            if (isset($request->strict) && $request->strict == 1 ){
                $menu = $menu->whereIn('type_menu_id', UserTools::getUserTypeMenuIds()) ;
            }
            $menu = $menu->orderBy('date')
                ->get()
                ->groupBy(function ($val) {
                    return Carbon::parse($val->date)->format('d-m-Y');
                });
            if (isset($request->export) && $request->export=='EXCEL' ){
                return Excel::download(new MenuOfWeekExport($menu), 'menu_semaine.xlsx');
            }

            return ApiResponseFormatTools::Format(true,'',$menu);

        }catch (\Exception $e){
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);

        }

    }


    // to send mail that menu of week is ready
    public static function menuMail($request){
        try {
            $date = new \DateTime($request->dateDebut);
            $week = intval($date->format("W")) ;
            $menu = Menu::find($week);

            $data = array(
                'dateDebut' => Carbon::parse($request->dateDebut)->format('d-m-Y'),
                'dateFin' => Carbon::parse($request->dateFin)->format('d-m-Y'),
            );

            $menuRepas = MenuRepas::whereBetween('date', [Carbon::parse($request->dateDebut)->startOfDay(), Carbon::parse($request->dateFin)->endOfDay()])->get();
            if (count($menuRepas) >0){
                $mails = User::where('profil',UserTools::USAGERPROFIL)->where('status',true)->pluck('email')->toArray();
                Mail::to($mails)->send(new SendNotifMenuOfWeek($data));
                $menu->update(['dateEnvoiMail' => Carbon::now(), 'mailsend' => true]);
                return ApiResponseFormatTools::Format(true,'');
            }
            return ApiResponseFormatTools::Format(false,'pas de menu disponible pour cette période');

        }catch (\Exception $e){
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);
        }

    }

}

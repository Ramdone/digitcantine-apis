<?php


namespace App\Tools;


use App\Models\UserTableLog;
use Illuminate\Support\Facades\Auth;

class LogTools
{
    // create the log
    public static function createLog($user){

        $user = $user->allRelation()->first();
        $data = [
            'created_by' => Auth::user()->id,
            'user_id' => $user->id,
            'object_before' => $user
        ];
        $log = UserTableLog::create($data);
        return $log->id;
    }


    // update log
    public static function updateLog($id, $user){
        $user = $user->allRelation()->first();
        $data = [
            'object_after' => $user
        ];
        $log = UserTableLog::find($id);
        $log->update($data);
    }

}

<?php


namespace App\Tools;


use App\Models\Absence;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\Types\This;

class AbsenceTools
{
    // to create Absence for Usager
    public static function create($request){
        try {
            // check if user have already Absence Between theses dates
            $checkExist =  Absence::where('user_id',$request->user_id)
                ->where('status', true)
                ->where(function($query) use ($request){
                    $query->whereBetween('dateDebut',[$request->dateDebut, $request->dateFin])
                        ->orWhereBetween('dateFin',[$request->dateDebut, $request->dateFin]);
                })
                ->get()
            ;
            if (count($checkExist)){
                return ApiResponseFormatTools::Format(false,"L'usager à déja une absence dans cette période");
            }

            $request['created_by'] = Auth::user()->id;
            $absence = Absence::create($request->all());
            return ApiResponseFormatTools::Format(true,'',$absence);

        }catch (\Exception $e){
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);
        }

    }


    // to update Absence
    public static function update($request){
        try {
            $absence = Absence::find($request->id);
            if ($absence->dateDebut != $request->dateDebut){
                if ($absence->dateDebut >= Carbon::now()->startOfDay()){
                    return self::proccedUpdate($request,$absence);
                }
                return ApiResponseFormatTools::Format(false,"Vous ne pouvez pas modifier la date de debut de cette absence");
            }
            return self::proccedUpdate($request,$absence);

        }catch (\Exception $e){
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);
        }

    }


    // update procced
    private static function proccedUpdate($request,$absence){
        $checkExist =  Absence::where('user_id',$absence->user_id)
            ->where('status', true)
            ->where(function($query) use ($request){
                $query->whereBetween('dateDebut',[$request->dateDebut, $request->dateFin])
                    ->orWhereBetween('dateFin',[$request->dateDebut, $request->dateFin]);
            })
            ->where('id','!=',$request->id)
            ->get()
        ;
        if (count($checkExist)){
            return ApiResponseFormatTools::Format(false,"L'usager à déja une autre absence dans cette période");
        }
        return CrudTools::update(app('App\Models\Absence'),$request, $request->id);
    }

}

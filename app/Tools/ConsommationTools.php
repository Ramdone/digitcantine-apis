<?php


namespace App\Tools;



use App\Exports\AccessControl;
use App\Exports\ConsommationExport;
use App\Exports\MyConsommationExport;
use App\Models\Consommation;
use App\Models\Parametre;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ConsommationTools
{
    // to procced badge
    public static function doBadge($request){
        try {
            $user = User::where('codeBadge', $request->codeBadge)->first();
            $cantine = ComptoirTools::getCantine();

            if ($cantine->verify_access == true){
                $statusVerify = self::verifyBadge($user->matricule);
                if ($statusVerify == 1){
                    return self::procced($request,$user);
                }elseif ($statusVerify == 0){
                    return ApiResponseFormatTools::Format(false,"Cet usager n'a pas passé le control d'accès à l'usine à cette date");
                }elseif ($statusVerify == 3){
                    return ApiResponseFormatTools::Format(false,"Matricule non trouvé dans la base de l'usine");
                }
                else{
                    return ApiResponseFormatTools::Format(false,"Echec de connexion",[], 200);
                }
            }

            return self::procced($request,$user);

        }catch (\Exception $e) {
            return ApiResponseFormatTools::Format(false,$e->getMessage());
        }
    }


    // to continue badge process
    private static function procced($request,$user){
        $cantine = ComptoirTools::cantineStatutAndComptoir(true);
        $parametre = Parametre::firstOrCreate(['id'=>1]);

        if ($cantine['status'] == true){ // si la cantine est vraiment ouverte
            if(isset($cantine['myComptoir'])){ // si la session du comptoir est toujours vanide
                if (in_array($cantine['myComptoir']->menuRepas->type_menu_id, UserTools::getUserTypeMenuIds($user->id)) ){ // si l'usager à droit a ce menu
                    if (! isset($user->Absence)){ // si l'usager n'est pas en congé
                        if ($user->access_cantine == true){ // si l'accès a la cantine de l'usager n'est pas désactivé
                            $count = Consommation::where('user_id',$user->id)
                                ->whereBetween('created_at', [Carbon::now()->startOfDay()->format(UtilsTools::projectDateFormat()), Carbon::now()->endOfDay()->format(UtilsTools::projectDateFormat())])
                                ->count();
                            if ($count < (int) $user->nbr_repas){ // s'il na pas encore atteint sa limite de repas dasn une journée
                                $consomation = Consommation::create([
                                    'user_id' => $user->id,
                                    'ouverture_comptoir_id' => $request->myComptoir_id,
                                    'montant_repas' => $parametre->montant_repas,
                                    'nbr_repas' => $user->nbr_repas,
                                    'nbr_consommation' => $count + 1,
                                ]);
                                return ApiResponseFormatTools::Format(true,'', Consommation::find($consomation->id));
                            }
                            return ApiResponseFormatTools::Format(false,"Cet Usager a atteint sa limite de repas dans une journée");
                        }
                        return ApiResponseFormatTools::Format(false,"l'accès à la cantine de cet usager est désactivé");
                    }
                    return ApiResponseFormatTools::Format(false,"Cet usager est supposé ètre en congé");
                }
                return ApiResponseFormatTools::Format(false,"Cet usager n'a pas droit à ce type de menu");
            }
            return ApiResponseFormatTools::Format(false,"Votre session n'est plus valide");
        }
        return ApiResponseFormatTools::Format(false,"La cantine est fermé");
    }


    // to verify if badge is scanned in entrance
    private static function verifyBadge($code){
        try {
            $response = Http::get(env("VERIFY_BADGE_URL").$code);

            if ($response->ok()){
                $body = $response->json();
                /*if ($body['status'] == 1){
                    return 1;
                }
                return 0;*/
                return $body['response'];
            }
            return 4;
        }catch (\Exception $e){
            return 5;

        }

    }



    // to get list off all consommation for a specific day
    public static function consommationList ($request){
        try {
            $limit = UtilsTools::limit($request);
            $page = UtilsTools::page($request);
            $skip = UtilsTools::skip($page,$limit);

            $dateStart = isset($request->date) ? Carbon::parse($request->date)->startOfDay()->format(UtilsTools::projectDateFormat()) : Carbon::now()->startOfDay()->format(UtilsTools::projectDateFormat());
            $dateEnd = isset($request->date) ? Carbon::parse($request->date)->endOfDay()->format(UtilsTools::projectDateFormat()) : Carbon::now()->endOfDay()->format(UtilsTools::projectDateFormat());

            $consomations = Consommation::whereBetween('created_at', [$dateStart,$dateEnd])->orderby('created_at');
            if (isset($request->name)){
                $consomations = $consomations->whereRelation('usager', 'name', 'like', "%$request->name%");
            }

            if (isset($request->export) && $request->export=='EXCEL' ){
                return Excel::download(new ConsommationExport($consomations->get()), 'consommation.xlsx');
            }
            $all = $consomations->count();

            $consomations = $consomations->limit($limit)->skip($skip)->get();
            $lastpage = UtilsTools::lastPage($all,$limit);

            return ApiResponseFormatTools::FormatPaginate(true,'',$all,$limit,$page, $lastpage, $consomations);

        }catch (\Exception $e){
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);

        }

    }


   // to get connected user consommation list
    public static function myConsommationList($request){
        try {

            $limit = UtilsTools::limit($request);
            $page = UtilsTools::page($request);
            $skip = UtilsTools::skip($page,$limit);

            $dateStart = isset($request->date) ? Carbon::parse($request->date)->startOfDay()->format(UtilsTools::projectDateFormat()) : Carbon::now()->startOfDay()->format(UtilsTools::projectDateFormat());
            $dateEnd = isset($request->date) ? Carbon::parse($request->date)->endOfDay()->format(UtilsTools::projectDateFormat()) : Carbon::now()->endOfDay()->format(UtilsTools::projectDateFormat());


            $consomations = Consommation::where('user_id', Auth::user()->id)->whereBetween('created_at', [$dateStart,$dateEnd])->orderby('created_at');
            if (isset($request->export) && $request->export=='EXCEL' ){
                return Excel::download(new MyConsommationExport($consomations->get()), 'my_consommation.xlsx');
            }
            $all = $consomations->count();
            $consomations = $consomations->limit($limit)->skip($skip)->get();
            $lastpage = UtilsTools::lastPage($all,$limit);
            return ApiResponseFormatTools::FormatPaginate(true,'',$all,$limit,$page, $lastpage, $consomations);

        }catch (\Exception $e){
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);

        }


    }

}

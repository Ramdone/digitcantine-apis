<?php


namespace App\Tools;


use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class UtilsTools
{

    public static function limit(Request $request){
        $limit = isset($request->limit) ? (int)$request->limit : 10;
        return $limit;

    }


    public static function page(Request $request){
        $page = isset($request->page) ? (int)$request->page : 1;
        return $page;

    }


    public static function skip($page,$limit){
        $skip = ($page - 1) * $limit;
        return $skip;
    }

    public static function lastPage($all, $limil){
        $lastPage = $all /	$limil;
        return ceil($lastPage);

    }


    public static function datetimeFormat($date){
        $date_format = isset($date) ? Carbon::parse($date)->format('d-m-Y h:i:s') : '';
        return $date_format;
    }

    public static function dateFormat($date){
        $date_format = isset($date) ? Carbon::parse($date)->format('d-m-Y') : '';
        return $date_format;
    }


    public static function projectDateFormat(){
        if(env('DB_CONNECTION') == 'sqlsrv'){
            return 'd-m-Y H:i:s';
        }
        return 'Y-m-d H:i:s';
    }


    public static function getMonthLabel($id)
    {
        switch ((int)$id) {
            case 1:
                $label = 'Janvier';
                break;
            case 2:
                $label = 'Février';
                break;
            case 3:
                $label = 'Mars';
                break;
            case 4:
                $label = 'Avril';
                break;
            case 5:
                $label = 'Mai';
                break;
            case 6:
                $label = 'Juin';
                break;
            case 7:
                $label = 'Juillet';
                break;
            case 8:
                $label = 'Août';
                break;
            case 9:
                $label = 'Septembre';
                break;
            case 10:
                $label = 'Octobre';
                break;
            case 11:
                $label = 'Novembre';
                break;
            case 12:
                $label = 'Décembre';
                break;
        }
        return $label;
    }
}

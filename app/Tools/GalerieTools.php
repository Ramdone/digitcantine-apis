<?php


namespace App\Tools;


use App\Models\Galerie;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class GalerieTools
{
    // to upload food image into the galerie
    public static function uploadImageToGalerie($request){
        try {
            $directoryPath=public_path('galerie');
            //check if the directory exists
            if(!File::isDirectory($directoryPath)){
                //make the directory because it doesn't exists
                File::makeDirectory($directoryPath);
            }
            $fileName = time().'.'.$request->photo->extension();

            $request->photo->move($directoryPath, $fileName);
            $galerie = Galerie::create(['photo' => 'galerie/'.$fileName]);
            return ApiResponseFormatTools::Format(true,"",$galerie);

        }catch (\Exception $e){
            return ApiResponseFormatTools::Format(false,$e->getMessage());
        }

    }

}

<?php


namespace App\Tools;


use App\Models\DemandeControlAccess;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class AccessControlTools
{

    // tools to ask for cancelling control
    public static function askCancel($request){

        try {
            if (DemandeControlAccess::where('status', 0)->exists()){
                return ApiResponseFormatTools::Format(true,'Vous avez deja une demande en cour',DemandeControlAccess::where('status', 0)->first());
            }

            if (ComptoirTools::getCantine()->verify_access == false){
                return ApiResponseFormatTools::Format(false,'Le control est actuellement désactivé');
            }

            $request['created_by'] = Auth::user()->id;
            DemandeControlAccess::create($request->all());
            return ApiResponseFormatTools::Format(true,'');

        } catch (\Exception $e) {
            return ApiResponseFormatTools::Format(false,$e->getMessage());
        }
    }


    // actions on ask for cancelling
    public static function actions($request){

        try {
            $ligne = DemandeControlAccess::where('status', 0)->first();

            $request['validated_by'] = Auth::user()->id;
            $request['validated_at'] = Carbon::now();
            $ligne->update($request->all());
            if ($ligne->status == 1){
                ComptoirTools::getCantine()->update(['verify_access' => false]);
            }
            return ApiResponseFormatTools::Format(true,'');

        } catch (\Exception $e) {
            return ApiResponseFormatTools::Format(false,$e->getMessage());
        }
    }

}

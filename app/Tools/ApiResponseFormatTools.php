<?php


namespace App\Tools;


class ApiResponseFormatTools
{
    // respone format for APIs
    public static function Format($status,$message='',$data=[],$code=200){

        return response()->json([
            'success'=> $status,
             'message'=> $message ,
            'data' => $data],$code);

    }


    // respone format for APIs with pagination
    public static function FormatPaginate($status, $message='', $total = 0, $limit = 0, $page = 0, $lastpage = 0,$data=[],$code=200){

        return response()->json([
            'success'=> $status,
            'message'=> $message ,
            "total" => $total,
            "limit" => $limit,
            "page" =>   $page,
            "last_page" =>   $lastpage,
            'data' => $data],$code);

    }

}

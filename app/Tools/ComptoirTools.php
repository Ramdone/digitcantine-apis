<?php


namespace App\Tools;


use App\Exports\ConsommationExport;
use App\Exports\OpenComptoirExport;
use App\Models\Cantine;
use App\Models\OuvertureComptoir;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ComptoirTools
{
    // tools to open comptoir
    public static function openComptoir($request){
        DB::beginTransaction();
        try {
            foreach ($request->datas as $item) {
                $item['dateDebut'] = Carbon::parse($item['dateDebut'])->format(UtilsTools::projectDateFormat()) ;
                $item['dateFin'] = Carbon::parse($item['dateFin'])->format(UtilsTools::projectDateFormat()) ;
                if (self::verification($item, 'userAffect') == true){
                    if (self::verification($item, 'comptoirAffect') == true){
                        if (self::verification($item, 'menuAffect') == true){
                            $item['created_by']= Auth::user()->id;
                            OuvertureComptoir::create($item);
                        }else{
                            DB::rollBack();
                            return ApiResponseFormatTools::Format(false,'Un des menus est déja est attribié a un autre comptoir dans la mème période');
                        }
                    }else{
                        DB::rollBack();
                        return ApiResponseFormatTools::Format(false,'Un des comptoirs est déja attribué dans la même période');
                    }
                }else{
                    DB::rollBack();
                    return ApiResponseFormatTools::Format(false,'Un des restaurateurs a déja une affection dans la même période');
                }
            }

            DB::commit();
            return ApiResponseFormatTools::Format(true,'');

        }catch (\Exception $e){
            DB::rollBack();
            return ApiResponseFormatTools::Format(false,$e->getMessage());

        }
    }



    // condition to verify before affect user to comptoir
    public static function verification($item,$type){
        if ($type == 'userAffect'){
            $checkAffect = OuvertureComptoir::where('user_aff_id', $item['user_aff_id']);
        }

        if ($type == 'comptoirAffect'){
            $checkAffect = OuvertureComptoir::where('comptoir_id', $item['comptoir_id']);
        }

        if ($type == 'menuAffect'){
            $checkAffect = OuvertureComptoir::where('menuRepas_id', $item['menuRepas_id']);
        }

        $checkAffect = $checkAffect->where(function ($query) use ($item) {
            $query->whereBetween('dateDebut', [$item['dateDebut'], $item['dateFin']])
                ->orwhereBetween('dateFin', [$item['dateDebut'], $item['dateFin']]);
        })->get();
        if (count($checkAffect)){
            return false;
        }
        return true;
    }



    // tool to get comptoir program list
    public static function comptoirProgram($request){
        try {
            $limit = UtilsTools::limit($request);
            $page = UtilsTools::page($request);
            $skip = UtilsTools::skip($page,$limit);

            $dateStart = isset($request->dateDebut) ? Carbon::parse($request->dateDebut)->startOfDay()->format(UtilsTools::projectDateFormat())  : Carbon::now()->startOfDay()->format(UtilsTools::projectDateFormat()) ;
            $dateEnd = isset($request->dateFin) ? Carbon::parse($request->dateFin)->endOfDay()->format(UtilsTools::projectDateFormat())  : Carbon::now()->endOfDay()->format(UtilsTools::projectDateFormat()) ;

            $liste = OuvertureComptoir::where('dateDebut','>=', $dateStart);
            if (isset($request->dateFin)){
                $liste = $liste->where('dateFin','<=',$dateEnd);
            }

            if (isset($request->export) && $request->export=='EXCEL' ){
                return Excel::download(new OpenComptoirExport($liste->get()), 'liste_programmation.xlsx');
            }
            $all = $liste->count();

            $liste = $liste->limit($limit)->skip($skip)->get();
            $lastpage = UtilsTools::lastPage($all,$limit);
            return ApiResponseFormatTools::FormatPaginate(true,'',$all,$limit,$page, $lastpage, $liste);

        }catch (\Exception $e){
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);
        }
    }



    // to update programming comptoir
    public static function update($request){
        try {
            $request['dateDebut'] = Carbon::parse($request->dateDebut)->format(UtilsTools::projectDateFormat()) ;
            $request['dateFin'] = Carbon::parse($request->dateFin)->format(UtilsTools::projectDateFormat()) ;
            $ouvertureComptoir = OuvertureComptoir::find($request->id);

            if ($ouvertureComptoir->dateDebut != $request->dateDebut){
                if ($ouvertureComptoir->dateDebut >= Carbon::now()->startOfDay()){
                    return self::proccedUpdate($request,$ouvertureComptoir);
                }
                return ApiResponseFormatTools::Format(false,"Vous ne pouvez pas modifier la date de debut d'une session déja en cour");
            }
            return self::proccedUpdate($request,$ouvertureComptoir);

        } catch (\Exception $e) {
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);
        }

    }


    //update programing comptoir
    public static function proccedUpdate($request,$ouvertureComptoir){

        if (self::updateVerification($request,$ouvertureComptoir, 'userAffect') == true){
            if (self::updateVerification($request,$ouvertureComptoir, 'comptoirAffect') == true){
                if (self::updateVerification($request,$ouvertureComptoir, 'menuAffect') == true){
                    $ouvertureComptoir->update($request->all());
                    return ApiResponseFormatTools::Format(true,'Modification éffectuée avec succes');

                }else{
                    return ApiResponseFormatTools::Format(false,'Le menu est déja est attribié a un autre comptoir dans cette période');
                }
            }else{
                return ApiResponseFormatTools::Format(false,'Le comptoir est déja attribué dans dans cette période');
            }
        }else{
            return ApiResponseFormatTools::Format(false,'Le restaurateur a déjâ une affectation dans cette période');
        }

    }


    // condition to  verify before update programing comptoir
    public static function updateVerification($request,$ouvertureComptoir,$type ){
        if ($type == 'userAffect'){
            $checkAffect = OuvertureComptoir::where('user_aff_id', $ouvertureComptoir->user_aff_id);
        }

        if ($type == 'comptoirAffect'){
            $checkAffect = OuvertureComptoir::where('comptoir_id', $ouvertureComptoir->comptoir_id);
        }

        if ($type == 'menuAffect'){
            $checkAffect = OuvertureComptoir::where('menuRepas_id', $ouvertureComptoir->menuRepas_id);
        }

        $checkAffect = $checkAffect->where(function ($query) use ($request) {
            $query->whereBetween('dateDebut', [$request->dateDebut, $request->dateFin])
                ->orwhereBetween('dateFin', [$request->dateDebut, $request->dateFin]);
        })
        ->where('id','!=',$request->id)
        ->get();
        if (count($checkAffect)){
            return false;
        }
        return true;
    }



   // to close comptoir
    public static function closeComptoir($request){
        try {
            $cantine = self::getCantine();
            if ($cantine->status == true){
                $openComtpoir = OuvertureComptoir::where('session',$cantine->session)->get();
                $hour = Carbon::now()->format('h:i:s');

                $data = array();
                $data['closed_by'] = Auth::user()->id;
                $data['heure_fin'] = $hour;
                foreach ($openComtpoir as $item){
                    $item->update($data);
                }

                $cantine->update(array('status' => false,'verify_access' => true, 'session' => null));
                return ApiResponseFormatTools::Format(true,'');

            }
            return ApiResponseFormatTools::Format(false,'Comptoir are already closed');

        }catch (\Exception $e){
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);
        }

    }


    // to get cantine status and menu on each comptoir
    public static function cantineStatutAndComptoir($returnData = null){
        try {
         //   $cantine =
            $status = false;
            $myComptoir = null;
            $openComtpoir = OuvertureComptoir::where('dateDebut','<=',Carbon::now()->format(UtilsTools::projectDateFormat()) )
                ->where('dateFin','>=',Carbon::now()->format(UtilsTools::projectDateFormat()) )
                ->get();

            if (count($openComtpoir)){
                $status = true;
                $myComptoir = OuvertureComptoir::where('user_aff_id', Auth::user()->id)
                    ->where('dateDebut','<=',Carbon::now()->format(UtilsTools::projectDateFormat()) )
                    ->where('dateFin','>=',Carbon::now()->format(UtilsTools::projectDateFormat()) )
                    ->first();
            }else{
                $openComtpoir = null;
            }

            $data = [
                'status' => $status,
                'openComtpoir' => $openComtpoir,
                'myComptoir' =>$myComptoir
            ];

            if ($returnData){
                return $data;
            }

            return ApiResponseFormatTools::Format(true,'',$data);

        } catch (\Exception $e) {
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);
        }
    }

    public static function getCantine(){
        $cantine = Cantine::firstOrCreate(['id' => 1]);
        return $cantine;

    }

}

<?php


namespace App\Tools;


class CustumValidatorMessages
{
    public static function message(){
        return [
            'datas.required' => "Veuillez remplir les élémnents qu'il faut pour ouvrir au moins un comptoir",
            'datas.array' => "Veuillez remplir les élémnents qu'il faut pour ouvrir au moins un comptoir",
            'datas.min' => "Veuillez remplir les élémnents qu'il faut pour ouvrir au moins un comptoir",

            'datas.*.menuRepas_id.required' => 'Veuillez sélectionner le menu',
            'datas.*.menuRepas_id.distinct' => "Vous ne pouvez pas affecter un même repas à plusieurs comptoirs à la fois ",
            'datas.*.menuRepas_id.exists' => "Le menu que vous avez sélcetionné n'exite pas",

            'menuRepas_id.distinct' => "Vous ne pouvez pas affecter un même repas à plusieurs comptoirs à la fois ",
            'menuRepas_id.exists' => "Le menu que vous avez sélcetionné n'exite pas",


            'datas.*.user_aff_id.required' => 'Veuillez sélectionner le restaurateur',
            'datas.*.user_aff_id.distinct' => "Vous ne pouvez pas affecter un même restaurateur à plusieurs comptoirs à la fois ",
            'datas.*.user_aff_id.exists' => "Le restaurateur que vous avez sélcetionné n'exite pas",

            'datas.*.comptoir_id.required' => 'Veuillez sélectionner le comptoir',
            'datas.*.comptoir_id.distinct' => "Vous ne pouvez pas ouvrir le même comptoir plusieur fois",
            'datas.*.comptoir_id.exists' => "Un des comptoirs que vous essayez d'ouvrir n'exixter",

            'datas.*.dateDebut.required' => 'La date de début est requise',
            'datas.*.dateDebut.date_format' => 'Veuillez vérifer le format de la date de début',
            'datas.*.dateDebut.after_or_equal' => 'La date de début doit être supérieur ou égale à :date',

            'datas.*.dateFin.required' => 'La date de fin est requise',
            'datas.*.dateFin.date_format' => 'Veuillez vérifer le format de la date de fin',
            'datas.*.dateFin.after' => 'La date et heure de fin ne peut pas ètre inférieur au :date',

            'typemenu_id.required' => "Aucun type de menu auquel l'usager a droit n'a été slectionné",
            'typemenu_id.array' => "Veuillez selectionner les types de menu auxquels l'usager a droit",
            'typemenu_id.*.required' => "Aucun type de menu auquel l'usager a droit n'a été slectionné",
            "typemenu_id.*.distinct"  => "Vous ne pouvez selectionner le même type de menu plusieurs fois",
            "typemenu_id.*.exists"  => "Un ou plusieurs types de menu sont invalides",


            'user_id.required' => "Veuillez sélectonner l'usager",
            'user_id.exists' => "L'usager n'existe pas",
            'motif.required' => 'Veuillez saisir le motif',
            'typeAbsence_id.required' => "Veuillez sélectionner le type de l'absence",
            'typeAbsence_id.exists' => "Le type d'absence que vous avez selectionné n'existe pas",
            'dateDebut.required' => "La date de début est requise",
            'dateDebut.date_format' => "Veuillez vérifer le format de la date de début",
            'dateDebut.after_or_equal' => 'La date de début doit être supérieur ou égale à :date',
            'dateDebut.after' => "La date de début ne peut pas ètre inférieur au :date ",

            'dateFin.required' => "La date de fin est requise",
            'dateFin.date_format' => "Veuillez vérifer le format de la date de fin",
            'dateFin.after' => "La date de fin ne peut pas ètre inférieur au :date ",
            'dateFin.after_or_equal' => 'La date de fin doit être supérieur ou égale à :date',
            'id.required' => "Le champ id est requise",
            'id.exists' => "Le id est ivalide",
            'update_motif.required' => "Le motif de modification est obligatoire",

            'status.required' => 'Le status est requis',
            'status.in' => 'La valeur de statut est incorrecte',
            'motifRejet.required_if'=> "Veuillez saisir le motif de rejet",

            'name.required' => "Le nom est requis",
            'name.max' => 'Le nom ne peut pas depasser :max caractères',
            'nom.required' => "Le nom est requis",
            'nom.max' => 'Le nom ne peut pas depasser :max caractères',
            'prenom.required' => "Le prénom est requis",
            'prenom.max' => 'Le prénom ne peut pas depasser :max caractères',
            'email.required' => "Le email est requis",
            'email.email' => "Le email n'est pas un email valide",
            'email.max' => 'Le email ne peut pas depasser :value caractères',
            'email.unique' => 'Cet email est déja attribué à un autre usager',
            'email.exists' => "L'email ne correspond pas à aucun usager",
            'codeRole.required' => 'Le role est requis',
            'codeRole.in' => 'Le role sélectionner est incorrecte',
            'url.required' => 'Le lien de redirection est requis',
            'url.url' => "Le lien de redirection n'est pas un lien valide",
            'matricule.required' => 'Le matricule est requis',
            'matricule.max' => 'Le matricule ne peut pas dépasser :max caractères',
            'matricule.unique' => 'Ce matricule est déja attribué à un autre usager',
            'codeBadge.required' => 'Le code du badge est requis',
            'codeBadge.min' => 'Le code du badge doit ètre au moins :min caractères',
            'codeBadge.unique' => 'Ce code de badge est déja attribué à un autre usager',
            'codeBadge.exists' => 'Ce code de cet badge ne correspond à aucun usager',
            'poste_id.required' => "Le poste de l'usager est requis",
            'poste_id.exists' => "Le poste sélectionné n'existe pas",
            'type_id.required' => "Le type de l'usager est requis",
            'type_id.exists' => "Le type sélectionné n'existe pas",
            'structure_id.required' => "La structure de l'usager est requise",
            'structure_id.exists' => "La structure sélectionnée n'existe pas",
            'categorie_id.required' => "La catégorie de l'usager est requise",
            'categorie_id.exists' => "La catégorie sélectionnée n'existe pas",
            'service_id.required' => "Le service est requis",
            'service_id.exists' => "La service sélectionnée n'existe pas",


            'badge.required' => 'Le code du badge est requis',
            'badge.min' => 'Le code du badge doit ètre au moins :min caractères',
            'badge.unique' => 'Ce code de badge est déja attribué à un autre usager',
            'badge.exists' => 'Ce code de cet badge ne correspond à aucun usager',
            'poste.required' => "Le poste de l'usager est requis",
            'poste.exists' => "Le poste sélectionné n'existe pas",
            'type.required' => "Le type de l'usager est requis",
            'type.exists' => "Le type sélectionné n'existe pas",
            'structure.required' => "La structure de l'usager est requise",
            'structure.exists' => "La structure sélectionnée n'existe pas",
            'categorie.required' => "La catégorie de l'usager est requise",
            'categorie.exists' => "La catégorie sélectionnée n'existe pas",
            'service.required' => "Le service de l'usager est requis",
            'service.exists' => "Le service sélectionné n'existe pas",


            'password.required' => 'le mot de passe est requis',
            'curent_password.required' => 'le mot de passe actuel est requis',
            'new_password.required' => 'le nouveau mot de passe est requis',
            'password.min' => 'le mot de passe doit ètre au moins :min caractères',
            'new_password.min' => 'le nouveau mot de passe doit ètre au moins :min caractères',

            'libelle.required' => 'Le libellé est requis',
            'libelle.unique' => 'Ce libelle existe déja',


            'myComptoir_id.required' => "L'identifaint du comptoir est requis",
            'myComptoir_id.exists' => "Le comptoir n'est pas ouvert",
            'date.date' => "La date est n'est âs valide",

            'photo.required' => 'La photo est requise',
            'photo.mimes' => 'La photo doit ètre un fichier de type: :values.',
            'photo.max' => 'La photo ne doit pas avoir une capicité supérieur à :max Kb',

            'file.required' => 'Veuillez choisir le fichier à importer',
            'file.mimes' => 'La fichier doit ètre un fichier de type: :values.',

            'id.array' => "Veuillez sélectionner au moins un élément",
            'id.min' => "Veuillez sélectionner au moins un élément",
            'id.*.exists' => "Les éléments sélectionnés sont invalide",

            'typeRepas_id.required' => 'Le type du repas est requis',
            'typeRepas_id.exists' => 'Le type du repas séléctionné est invalide',
            'galerie_id.required' => 'Veuillez associé une image au repas',
            'galerie_id.exists' => "L'image attribuée n'existe pas dans la galerie",
            'elementRepas_id.required' => 'Les éléments du repas sont requis',
            'elementRepas_id.*.required' => 'Les éléments du repas sont requis',
            "elementRepas_id.*.distinct"  => "Vous ne pouvez pas attribué le même élément repas deux fois à un repas",
            "elementRepas_id.*.exists"  => "Un ou plusieurs éléments repas sélectionnés est invalide",

            'date.required' => 'La date est requis',
            'date.date_format' => 'Veuillez vérifier le format de la date',
            'date.after_or_equal' => 'La date doit être supérieur ou égale à :date',
            'repas_id.required' => 'Veuillez séléctionné le repas',
            "repas_id.*.distinct"  => "Vous ne pouvez pas ajouter le même repas deux fois pour la même journée",
            "repas_id.*.exists"  => "Un ou plusieurs repas sélectionnés est invalide",

            'repas.*.repas_id.required' => 'Veuillez séléctionné le repas',
            "repas.*.repas_id.distinct"  => "Vous ne pouvez pas ajouter le même repas deux fois pour la même journée",
            "reprepas.*.repas_idas_id.exists"  => "Un ou plusieurs repas sélectionnés est invalide",

            'repas.*.type_menu_id.required' => 'Veuillez séléctionné le type de menu',
            "reprepas.*.type_menu_id.exists"  => "Un ou plusieurs type de menu sélectionnés est invalide",
            "repas.*.reserv_is_consom.exists"  => "Veuillez préciser si la réservation équivaut à la connsommation",

        ];
    }

}

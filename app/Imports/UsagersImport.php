<?php

namespace App\Imports;

use App\Models\User;
use App\Tools\UserTools;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Validators\Failure;


use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsFailures;

class UsagersImport implements ToCollection, WithHeadingRow, WithValidation,SkipsOnFailure
{
    use  SkipsFailures;


    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
            foreach ($rows->toArray() as $row){
                $password = UserTools::generatePassword();
                $row['password'] = bcrypt($password);
                $row['codeRole'] = 'UG';
                $row['profil'] = UserTools::USAGERPROFIL;
                $row['created_by'] = Auth::user()->id;
             //   User::create($row->toArrar());
            }


         //

    }


    /**
     * @return array
     */
    public function rules() : array
    {
        return [
            '*.nom' => 'required|distinct|string|max:255',
            '*.name' => 'required|distinct|string|max:255',
            '*.email' => 'required|distinct|string|email|max:255|unique:users',
            '*.matricule' => 'required|distinct|max:5|unique:users',
            '*.badge' => 'required|distinct|unique:users,codeBadge',
        ];
    }


    /**
     * @param Failure[] $failures
     */
    public function onFailure(Failure ...$failures)
    {
        $this->failures = array_merge($this->failures, $failures);
        return $this->failures;
        /*foreach ($failures as $failure) {
            dd($failure->errors());
            $failure->row(); // row that went wrong
            $failure->attribute(); // either heading key (if using heading row concern) or column index
            $failure->errors(); // Actual error messages from Laravel validator
            $failure->values(); // The values of the row that has failed.
        }
        dd($failures);*/
        // Handle the failures how you'd like.

    }

    public function failures()
    {
        return $this->failures;
    }



}

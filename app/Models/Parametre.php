<?php

namespace App\Models;

use App\Tools\UtilsTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Parametre extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $fillable = ['modal_show','delai_annulation_reservation','montant_repas'];
    protected $dateFormat =  null  ;

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->dateFormat = UtilsTools::projectDateFormat();
    }


    protected $casts = [
        'modal_show' => 'boolean',
        'montant_repas' => 'float'
    ];
}

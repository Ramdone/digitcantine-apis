<?php

namespace App\Models;

use App\Tools\UtilsTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cantine extends Model
{
    use HasFactory;
    protected $table = 'cantines';

    protected $guarded = ['id'];
    protected $fillable = ['libelle','status','verify_access','session'];

    protected $casts = [
        'status' => 'boolean',
    ];

    protected $dateFormat =  null  ;

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->dateFormat = UtilsTools::projectDateFormat();
    }



    public function openComptoir(){
        return $this->hasMany(OuvertureComptoir::class,"session", "session");
    }
}

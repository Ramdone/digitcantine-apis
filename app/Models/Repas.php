<?php

namespace App\Models;

use App\Tools\UtilsTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Repas extends Model
{
    use HasFactory;
    protected $table = 'repas';

    protected $guarded = ['id'];
    protected $fillable = ['libelle', 'typeRepas_id', 'galerie_id','status', 'created_by', 'deleted_by', 'deleted_at', 'updated_by'];
    protected $dateFormat =  null  ;

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->dateFormat = UtilsTools::projectDateFormat();
    }


    protected $with = ['typerepas','photo','composerRepas'];


    protected $casts = [
        'status' => 'boolean',
    ];

    public function typerepas(){
        return $this->belongsTo(TypeRepas::class,"typeRepas_id");
    }

    public function photo(){
        return $this->belongsTo(Galerie::class,"galerie_id");
    }

    public function composerRepas(){
        return  $this->hasMany(ComposerRepas::class,"repas_id", "id") ;
    }
}

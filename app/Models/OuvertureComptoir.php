<?php

namespace App\Models;

use App\Tools\UtilsTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OuvertureComptoir extends Model
{
    use HasFactory;
    protected $table = 'ouverture_comptoirs';

    protected $guarded = ['id'];
    protected $fillable = ['menuRepas_id','user_aff_id','comptoir_id','dateDebut', 'dateFin','created_by'];
    protected $dateFormat =  null  ;

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->dateFormat = UtilsTools::projectDateFormat();
    }

    protected $with = ['comptoir','menuRepas','agent'];



    public function menuRepas(){
        return $this->belongsTo(MenuRepas::class,"menuRepas_id");
    }

    public function comptoir(){
        return $this->belongsTo(Comptoir::class,"comptoir_id");
    }

    public function agent(){
        return $this->belongsTo(User::class,"user_aff_id");
    }
}

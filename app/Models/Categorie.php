<?php

namespace App\Models;

use App\Tools\UserTools;
use App\Tools\UtilsTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    use HasFactory;
    protected $table = 'categories';

    protected $guarded = ['id'];
    protected $fillable = ['libelle','status','created_by','deleted_by', 'deleted_at','updated_by'];

    protected $casts = [
        'status' => 'boolean',
    ];

    protected $dateFormat =  null  ;

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->dateFormat = UtilsTools::projectDateFormat();
    }


    public function Usager(){
        return $this->hasMany('App\Models\User',"categorie_id","id")
            ->where('users.status',true);
           // ->where('users.profil',UserTools::USAGERPROFIL);
    }

}

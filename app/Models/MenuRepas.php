<?php

namespace App\Models;

use App\Tools\UtilsTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class MenuRepas extends Model
{
    use HasFactory;
    protected $table = 'menu_repas';

    protected $guarded = ['id'];
    protected $fillable = ['menu_id','repas_id','date','type_menu_id','reserv_is_consom'];

    protected $dateFormat =  null  ;

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->dateFormat = UtilsTools::projectDateFormat();
    }


    protected $with = ['repas','typemenu'];

    protected $appends = ['Reserved'];


    public function repas(){
        return $this->belongsTo(Repas::class,"repas_id");
    }

    public function typemenu(){
        return $this->belongsTo(TypeMenu::class,"type_menu_id");
    }


    public function getReservedAttribute(){
        if (Reservation::where('menuRepas_id', $this->id)
            ->where('created_by', Auth::user()->id)
            ->first() ){
            return true;
        }
        return false;

    }
}

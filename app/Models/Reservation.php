<?php

namespace App\Models;

use App\Tools\UtilsTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    use HasFactory;
    protected $table = 'reservations';

    protected $guarded = ['id'];
    protected $fillable = ['menuRepas_id', 'created_by','dateReservedFor'];
    protected $dateFormat =  null  ;

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->dateFormat = UtilsTools::projectDateFormat();
    }


    public function menuRepas(){
        return $this->belongsTo(MenuRepas::class,"menuRepas_id");
    }

    public function usager(){
        return $this->belongsTo(User::class,"created_by") ->with(['structure']);
    }
}

<?php

namespace App\Models;

use App\Tools\UtilsTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeAbsence extends Model
{
    use HasFactory;
    protected $table = 'type_absences';

    protected $guarded = ['id'];
    protected $fillable = ['libelle','status','created_by','deleted_by', 'deleted_at','updated_by'];
    protected $dateFormat =  null  ;

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->dateFormat = UtilsTools::projectDateFormat();
    }


    protected $casts = [
        'status' => 'boolean',
    ];

}

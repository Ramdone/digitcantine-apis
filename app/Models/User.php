<?php

namespace App\Models;

use App\Tools\UtilsTools;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name','prenom','phone', 'email', 'password','default_password_change','profil','codeRole', 'photo','matricule', 'status',
        'codeBadge', 'access_cantine', 'motif_access_denied','nbr_repas','created_by','deleted_by',
        'deleted_at','updated_by','poste_id','type_id','structure_id','categorie_id','service_id'
    ];

    protected $appends = ['absence'];


    protected $dateFormat =  null  ;

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->dateFormat = UtilsTools::projectDateFormat();
    }


    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'Absence'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'status' => 'boolean',
        'default_password_change' => 'boolean',
        'access_cantine' => 'boolean',

    ];


    public function scopeAllRelation($query){
        $query->where('id', $this->id)->with('categorie','poste','type','structure','service','role','usertypemenu');
    }


    public function categorie(){
        return $this->belongsTo(Categorie::class,"categorie_id");
    }

    public function poste(){
        return $this->belongsTo(Poste::class,"poste_id");
    }

    public function type(){
        return $this->belongsTo(Type::class,"type_id");
    }

    public function structure(){
        return $this->belongsTo(Structure::class,"structure_id");
    }

    public function service(){
        return $this->belongsTo(Service::class,"service_id");
    }

    public function role(){
        return $this->belongsTo(Role::class,"codeRole",'code');
    }

    public function usertypemenu(){
        return $this->hasMany('App\Models\UserTypeMenu',"user_id","id");
    }


    public function getabsenceAttribute(){
        return Absence::where('user_id', $this->id)
            ->where('status',true)
            ->where('dateDebut','<=',Carbon::now()->format(UtilsTools::projectDateFormat()))
            ->where('dateFin','>=',Carbon::now()->format(UtilsTools::projectDateFormat()))
            ->first();
    }
}

<?php

namespace App\Models;

use App\Tools\UtilsTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DemandeControlAccess extends Model
{
    use HasFactory;
    protected $table = 'demande_control_access';

    protected $guarded = ['id'];
    protected $fillable = ['motif','status','motifRejet','created_by','validated_by','validated_at', 'updated_by'];
    // status => 0= en attente, 1 = valider, 2 = rejeter

    protected $dateFormat =  null  ;

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->dateFormat = UtilsTools::projectDateFormat();
    }


    protected $with = ['demandeur','valideur'];


    public function demandeur(){
        return $this->belongsTo(User::class,"created_by");
    }

    public function valideur(){
        return $this->belongsTo(User::class,"validated_by");
    }
}

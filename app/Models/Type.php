<?php

namespace App\Models;

use App\Tools\UserTools;
use App\Tools\UtilsTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    use HasFactory;
    protected $table = 'types';

    protected $guarded = ['id'];
    protected $fillable = ['libelle','status','created_by','deleted_by', 'deleted_at','updated_by'];
    protected $dateFormat =  null  ;

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->dateFormat = UtilsTools::projectDateFormat();
    }


    protected $casts = [
        'status' => 'boolean',
    ];

    public function Usager(){
        return $this->hasMany('App\Models\User',"type_id","id")
            ->where('users.status',true);
         //   ->where('users.profil',UserTools::USAGERPROFIL);
    }
}

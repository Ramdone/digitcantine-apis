<?php

namespace App\Models;

use App\Tools\UtilsTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Absence extends Model
{
    use HasFactory;
    protected $table = 'absences';

    protected $guarded = ['id'];
    protected $fillable = ['user_id','dateDebut','dateFin','status','motif','typeAbsence_id',
    'delete_motif','update_motif','created_by','deleted_by','deleted_at','updated_by'];

    protected $casts = [
        'status' => 'boolean',
    ];

    protected $dateFormat =  null  ;

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->dateFormat = UtilsTools::projectDateFormat();
    }

}

<?php

namespace App\Models;

use App\Tools\UtilsTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ComposerRepas extends Model
{
    use HasFactory;
    protected $table = 'composer_repas';

    protected $guarded = ['id'];
    protected $fillable = ['repas_id','elementRepas_id'];

    protected $with = ['element'];
    protected $dateFormat =  null  ;

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->dateFormat = UtilsTools::projectDateFormat();
    }


    public function element(){
        return $this->belongsTo(ElementRepas::class,"elementRepas_id");
    }
}

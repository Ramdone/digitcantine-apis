<?php

namespace App\Models;

use App\Casts\Json;
use App\Tools\UtilsTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserTableLog extends Model
{
    use HasFactory;
    protected $table = 'user_table_logs';

    protected $guarded = ['id'];
    protected $fillable = ['created_by','user_id','object_before','object_after'];
    protected $dateFormat =  null  ;
    protected $with = ['usager','createur'];


    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->dateFormat = UtilsTools::projectDateFormat();
    }


    protected $casts = [
        'object_before' => Json::class,
        'object_after' => Json::class,
    ];

    public function usager(){
        return $this->belongsTo(User::class,"user_id");
    }

    public function createur(){
        return $this->belongsTo(User::class,"created_by");
    }


}

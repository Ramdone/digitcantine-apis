<?php

namespace App\Models;

use App\Tools\UtilsTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserTypeMenu extends Model
{
    use HasFactory;
    protected $table = 'user_type_menus';

    protected $guarded = ['id'];
    protected $fillable = ['user_id', 'type_menu_id'];
    protected $dateFormat =  null  ;

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->dateFormat = UtilsTools::projectDateFormat();
    }

    protected $with = ['typemenu'];


    public function typemenu(){
        return $this->belongsTo(TypeMenu::class,"type_menu_id");
    }
}

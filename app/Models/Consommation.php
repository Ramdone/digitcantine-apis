<?php

namespace App\Models;

use App\Tools\UtilsTools;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Consommation extends Model
{
    use HasFactory;
    protected $table = 'consommations';

    protected $guarded = ['id'];
    protected $fillable = ['user_id', 'ouverture_comptoir_id','montant_repas','nbr_repas','nbr_consommation'];
    protected $dateFormat =  null  ;

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->dateFormat = UtilsTools::projectDateFormat();
    }

    protected $with = ['ouvertureComptoir','usager','reservation'];


    public function ouvertureComptoir(){
        return $this->belongsTo(OuvertureComptoir::class,"ouverture_comptoir_id");
    }

    public function usager(){
        return $this->belongsTo(User::class,"user_id")
            ->with(['structure']);
    }

    public function reservation(){
        return  $this->belongsTo(Reservation::class,"user_id",'created_by')
         //   ->whereDate('dateReservedFor',Carbon::parse($this->created_at)->startOfDay()->format(UtilsTools::projectDateFormat()))
            ->whereDate('dateReservedFor',Carbon::parse($this->created_at)->startOfDay())
            ->with(['menuRepas'])
        ;
    }

    protected $casts = [
        'montant_repas' => 'float'
    ];
}

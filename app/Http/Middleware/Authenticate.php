<?php

namespace App\Http\Middleware;

use App\Tools\ApiResponseFormatTools;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('login');
        }
    }


    // if token is invalid
    protected function unauthenticated($request, array $guards)
    {
      /*  abort(response()->json([
            'status' => 'false',
            'message' => 'invalid token',], 401));*/

        abort(ApiResponseFormatTools::Format(false,'invalid token',[],401));
    }
}

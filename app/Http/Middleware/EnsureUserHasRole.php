<?php

namespace App\Http\Middleware;

use App\Tools\ApiResponseFormatTools;
use Closure;
use Illuminate\Http\Request;

class EnsureUserHasRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
  //   * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $role1=null, $role2=null, $role3=null, $role4=null, $role5=null)
    {
        if ($request->user()->status == true){
            if ( isset($role1) && $request->user()->codeRole == $role1) {
                return $next($request);
            }elseif ( isset($role2) && $request->user()->codeRole == $role2) {
                return $next($request);
            } elseif ( isset($role3) && $request->user()->codeRole == $role3) {
                return $next($request);
            } elseif ( isset($role4) && $request->user()->codeRole == $role4) {
                return $next($request);
            }
            elseif ( isset($role5) && $request->user()->codeRole == $role5) {
                return $next($request);
            }else{
                return ApiResponseFormatTools::Format(false,"Vous n'ètes pas autorisé à éffectué cette action",[],401);
            }

        }
        return ApiResponseFormatTools::Format(false,"Votre compte est desactvé, veuillez contacter l'admin",[],401);

    }
}

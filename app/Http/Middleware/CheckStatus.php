<?php

namespace App\Http\Middleware;

use App\Tools\ApiResponseFormatTools;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(auth()->check() && (auth()->user()->status == 0)){
            return ApiResponseFormatTools::Format(false,'Votre compte est désactivé',[],401);
        }

        return $next($request);
    }
}

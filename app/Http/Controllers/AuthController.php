<?php

namespace App\Http\Controllers;

use App\Tools\ApiResponseFormatTools;
use App\Tools\CustumValidatorMessages;
use App\Tools\LogTools;
use App\Tools\MailTools;
use App\Tools\UserTools;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;



class AuthController extends Controller
{
    // to create a user role AGENT
    public function createAgent(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'prenom' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'matricule' => 'nullable|max:5|unique:users',
            'codeBadge' => 'nullable|min:3|unique:users',
            'codeRole' => 'required|in:AF,AR,RS,AT',
            'typemenu_id' => 'array|min:1',
            'typemenu_id.*' => 'distinct|exists:type_menus,id',
            'url' => 'required|url',
        ],CustumValidatorMessages::message());

        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first());
        }
        return  UserTools::createAgent($request);
    }


    // to create a user role Usager
    public function createUsager(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'prenom' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'matricule' => 'required|max:5|unique:users',
            'codeBadge' => 'nullable|min:3|unique:users',
            'poste_id' => 'nullable|exists:postes,id',
            'type_id' => 'nullable|exists:types,id',
            'structure_id' => 'nullable|exists:structures,id',
            'categorie_id' => 'required|exists:categories,id',
            'typemenu_id' => 'nullable|array|min:1',
            'typemenu_id.*' => 'nullable|distinct|exists:type_menus,id',
            'service_id' => 'nullable|exists:services,id',
            'url' => 'required|url',
        ],CustumValidatorMessages::message());

        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first());
        }
        $request['codeRole'] = 'UG';
        return UserTools::createUsager($request);

    }


    // to login a user
    public function login(Request $request)
    {
        if (!Auth::attempt($request->only('email', 'password'))) {
            return ApiResponseFormatTools::Format(false,'Email et ou mot de passe incorrecte',[],200);
        }

        $user = User::where('email', $request['email'])->firstOrFail();
        if ($user->status == 0){
            return ApiResponseFormatTools::Format(false,'Votre compte est désactivé');
        }
        $token = $user->createToken('auth_token');

        $datas = [
            'access_token' => $token->plainTextToken,
            'token_type' => 'Bearer',
            'token_expire_at' => Carbon::parse($token->accessToken->created_at)->addDays(2),
            "user" => $user
        ];
        return ApiResponseFormatTools::Format(true,"",$datas);

    }


    // get connected user
    public function me(Request $request)
    {
        return ApiResponseFormatTools::Format(true,"", UserTools::getUser(Auth::user()->id));
    }


    // to send mail for password reset
    public function resetPasswordMail(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users',
            'url' => 'required|url',
        ],CustumValidatorMessages::message());

        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first(),[],200);
        }

        $user = User::where('email', $request['email'])->firstOrFail();
        return MailTools::sendResetPasswordMail($request, $user);

    }


    // to reset password
    public function resetPassword(Request $request){
        $validator = Validator::make($request->all(), [
            'password' => 'required|min:5',
        ],CustumValidatorMessages::message());
        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first(),[],200);
        }
        $user = $request->user();
        $user->update(['password'=>Hash::make($request->password),'default_password_change'=>true]);
        return ApiResponseFormatTools::Format(true,"Password reset successfully",$user);

    }


    // to change passsword
    public function changePassword(Request $request){

        $validator = Validator::make($request->all(), [
            'curent_password' => 'required',
            'new_password' => 'required|min:5',
          //  'new_password' => 'required|same:confirm_password|min:5',
        ],CustumValidatorMessages::message());
        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->frist(),[],422);
        }

        if (!Hash::check($request->curent_password, Auth::user()->password)){
            return ApiResponseFormatTools::Format(false,'Votre ancien mot de passe est incorrect');
        }
        else{
            if ($request->curent_password !=$request->new_password){
                Auth::user()->update(['password'=>Hash::make($request->new_password),'default_password_change'=>true]);
                return ApiResponseFormatTools::Format(true,"Password changed successfully");
            }
            return ApiResponseFormatTools::Format(false,"Votre ancien mot de passe et le nouveau mot de passe ne doivent pas être identique");
        }
    }

    // to update user
    public function updateUser(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ],CustumValidatorMessages::message());
        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->frist());
        }
        try {
            $user = Auth::user();
            $user->update($request->all());
            return ApiResponseFormatTools::Format(true,"",$user);

        }catch (\Exception $e){
            return ApiResponseFormatTools::Format(false,$e->getMessage());
        }

    }

}

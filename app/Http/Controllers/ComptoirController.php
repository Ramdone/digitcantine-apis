<?php

namespace App\Http\Controllers;

use App\Exports\InformationsExport;
use App\Models\OuvertureComptoir;
use App\Tools\ApiResponseFormatTools;
use App\Tools\ComptoirTools;
use App\Tools\CrudTools;
use App\Tools\CustumValidatorMessages;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class ComptoirController extends Controller
{
    // to create comptoir
    public function createComptoir(Request $request){
        $validator = Validator::make($request->all(), [
            'libelle' => 'required',
        ]);
        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first(),[],422);
        }
        $model= app('App\Models\Comptoir');
        return CrudTools::store($model, $request);
    }

    // to get comptoir list
    public function getComptoir(Request $request){
        try {
            $liste = CrudTools::get(app('App\Models\Comptoir'),$request);
            if (isset($request->export) && $request->export=='EXCEL' ){
                return Excel::download(new InformationsExport($liste), 'informations.xlsx');
            }
            return ApiResponseFormatTools::Format(true,'',$liste);
        } catch (\Exception $e) {
            return ApiResponseFormatTools::Format(false,$e->getMessage());
        }
    }

// to delete specifique comptoir
    public function deleteComptoir(Request $request, $id){
        return CrudTools::delete(app('App\Models\Comptoir'),$request, $id);
    }



    // to open comptoir
    public function openComptoir(Request $request){
        $validator = Validator::make($request->all(), [
            'datas' => 'required|array|min:1',
            'datas.*.menuRepas_id' => 'required|exists:menu_repas,id',
            'datas.*.user_aff_id' => 'required|exists:users,id',
            'datas.*.comptoir_id' => 'required|exists:comptoirs,id',
            'datas.*.dateDebut' => 'required|date_format:d-m-Y H:i|after_or_equal:'.date('d-m-Y h:i'),
            'datas.*.dateFin' => 'required|date_format:d-m-Y H:i|after:datas.*.dateDebut',

        ],CustumValidatorMessages::message());
        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first());
        }

        return ComptoirTools::openComptoir($request);

    }


    // to get programming comptoit
    public function getComptoirProgram(Request $request){
        $validator = Validator::make($request->all(), [
            'dateDebut' => 'nullable|date_format:d-m-Y',
            'dateFin' => 'nullable|date_format:d-m-Y|after_or_equal:dateDebut',

        ],CustumValidatorMessages::message());
        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first());
        }
        return ComptoirTools::comptoirProgram($request);
    }


    // to update program comptoir
    public function updateComptoirProgram(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required|exists:ouverture_comptoirs',
            'dateDebut' => 'date_format:d-m-Y H:i|after_or_equal:'.date('d-m-Y h:i'),
            'dateFin' => 'date_format:d-m-Y H:i|after:dateDebut',
        ],CustumValidatorMessages::message());
        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first());
        }
        return ComptoirTools::update($request);
    }


    // to delete a program comptoir
    public function deleteComptoirProgram($id){
        try {
            $item = OuvertureComptoir::find($id);
            if (isset($item)){
                if ($item->dateDebut > Carbon::now()){
                    $item->delete();
                    return ApiResponseFormatTools::Format(true,'Suppression avec succès');
                }
                return ApiResponseFormatTools::Format(false,'Vous ne pouvez pas supprimer une session déja passé ou en cour');
            }
            return ApiResponseFormatTools::Format(false,'Non trouvé');
        } catch (\Exception $e) {
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);
        }
    }

    // close comptoir
    public function closeComptoir(Request $request){
        return ComptoirTools::closeComptoir($request);
    }


    // to get cantine status and list of menu
    public function getCantineStatus(Request $request){
        return ComptoirTools::cantineStatutAndComptoir();
    }

}

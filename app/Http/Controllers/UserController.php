<?php

namespace App\Http\Controllers;

use App\Exports\AgentExport;
use App\Exports\UsagersExport;
use App\Imports\UsagersImport;
use App\Models\Categorie;
use App\Models\Parametre;
use App\Models\Role;
use App\Models\User;
use App\Tools\ApiResponseFormatTools;
use App\Tools\CrudTools;
use App\Tools\CustumValidatorMessages;
use App\Tools\UserTools;
use App\Tools\UtilsTools;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    private $unvalidateRow = [];
    // to add user picture
    public function addUserProfil(Request $request){
        $validator = Validator::make($request->all(), [
            'photo' => 'required|mimes:jpg,jpeg,png|max:2048',
            'type' => 'required|in:NEW,UPDATE'
        ],CustumValidatorMessages::message());

        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first());
        }
        return UserTools::userProfil($request);
    }


    // to delete specific user
    public function deleteUser(Request $request, $id){
        return CrudTools::delete(app('App\Models\User'),$request, $id);
    }


    // to get utilities to create a User
    public function getDatas(Request $request){
        return ApiResponseFormatTools::Format(true,"",[
            "Types" => CrudTools::get(app('App\Models\Type'),$request),
            "Postes" => CrudTools::get(app('App\Models\Poste'),$request),
            "Structure" => CrudTools::get(app('App\Models\Structure'),$request),
            "Categorie" => CrudTools::get(app('App\Models\Categorie'),$request),
            "typeAbsence" => CrudTools::get(app('App\Models\TypeAbsence'),$request),
            "typeMenu" => CrudTools::get(app('App\Models\TypeMenu'),$request),
            "services" => CrudTools::get(app('App\Models\Service'),$request),
            "settings" => Parametre::firstOrCreate(['id'=>1]),
            "Roles" => Role::all(),
            'allComptoir'=> CrudTools::get(app('App\Models\Comptoir'),$request),
        ]);
    }


    // to get user Usager list
    public function getUsagers(Request $request){
        try {
            $limit = UtilsTools::limit($request);
            $page = UtilsTools::page($request);
            $skip = UtilsTools::skip($page,$limit);

            $liste = User::where('users.status', true)->where(function($query){
                $query->where('profil', UserTools::USAGERPROFIL)
                    ->orWhereIn('codeRole',['AT','AF']);
            });
            if (isset($request->name)){
                $liste = $liste->where('name', 'LIKE', "%$request->name%");
            }

            if (isset($request->categorie)){
                $liste = $liste->where('categorie_id',$request->categorie);
            }

            if (isset($request->access_cantine)){
                $liste = $liste->where('access_cantine',$request->access_cantine);
            }

            $all = $liste->count();

            $liste = $liste->orderby('name')->with(['categorie','poste','type','structure','usertypemenu','service']);

            if (isset($request->absence) && $request->absence == true ){
                if (isset($request->export) && $request->export=='EXCEL' ){
                    return Excel::download(new UsagersExport($liste->get()->filter(function ($value, $key){
                        return $value->absence != null;
                    })->values()), 'usagers.xlsx');
                }

                $all = $liste->get()->filter(function ($value, $key){
                    return $value->absence != null;
                })->count();

                $liste = $liste->limit($limit)->skip($skip)->get()->makeVisible(['Absence'])->filter(function ($value, $key){
                    return $value->absence != null;
                })->values();
            }else{
                if (isset($request->export) && $request->export=='EXCEL' ){
                    return Excel::download(new UsagersExport($liste->get()), 'usagers.xlsx');
                }

                $liste = $liste->limit($limit)->skip($skip)->get()->makeVisible(['Absence']);
            }

            $lastpage = UtilsTools::lastPage($all,$limit);
            return ApiResponseFormatTools::FormatPaginate(true,'',$all,$limit,$page, $lastpage, $liste);
        }catch (\Exception $e){
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);

        }

    }


    // to count usagers
    public function countUsager(Request $request){
        try {
            $model = app('App\Models\Type');
            $attribute = 'type_id';
            if (isset($request->categorie)){
                $model = app('App\Models\Categorie');
                $attribute = 'categorie_id';
            }

            $total = 0;
            $liste = $model::where('status', true)->with('Usager')->get();
            foreach ($liste as $cat){
                $cat['nbrAgent'] = isset($cat->Usager)  ? $cat->Usager->count() : 0;
                $total = $total + $cat['nbrAgent'];
            }

            $nonclasse = User::where('users.status',true)->whereNull($attribute)
                ->where(function($query){
                    $query->where('profil', UserTools::USAGERPROFIL)
                        ->orWhereIn('codeRole',['AT','AF']);
                })->count();

            if ($nonclasse>0){
                $liste[]= array('libelle' => 'Non classés', 'nbrAgent' => $nonclasse);
                $total = $total + $nonclasse;

            }

            return ApiResponseFormatTools::Format(true,'',['liste' =>$liste, 'total' => $total]);

        }catch (\Exception $e){
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);

        }



    }


    // import Usager
    public function importUsagers(Request $request){
        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:xls,xlsx',
            'url' => 'required|url',

        ],CustumValidatorMessages::message());

        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first());
        }

        DB::beginTransaction();
        try {
            $import = Excel::toArray(new UsagersImport(), $request->file('file'));
            $data = count($import) ? $import[0] : [];
            if (count($data) > 0){
                foreach ($data as $key=>$item){
                    $item['typemenu_id']= explode(',', $item['typemenu']);
                    if ($this->validateRow($item, $key)){
                        UserTools::importUsager($request,$item );
                    }
                }
            }else{
                return ApiResponseFormatTools::Format(false,'Le fichier est vide');
            }

            DB::commit();
            return ApiResponseFormatTools::Format(true,'',$this->unvalidateRow);

        }catch (\Exception $e){
            DB::rollBack();
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);

        }
    }


    // to  export usager list
    public function exportUsager(Request $request){
        try {
            return Excel::download(new UsagersExport(), 'usagers.xlsx');
        }catch (\Exception $e){
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);
        }

    }



    // to update access to cantine for usager
    public function updateAccesCantine(Request $request){
        $validator = Validator::make($request->all(), [
            'usager_id' => 'required|exists:users,id',
            'access_cantine' => 'required|in:0,1',
            'motif_access_denied'=> 'required_if:status,==,0'
        ],CustumValidatorMessages::message());

        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first());
        }

       return UserTools::updateUsager($request);

    }


    //update user status
    public function userStatus(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required|exists:users,id',
            'status' => 'required|in:0,1',
        ],CustumValidatorMessages::message());

        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first());
        }
        return CrudTools::update(app('App\Models\User'), $request, $request->id);
    }


    // to download file uplaod format
    public function download(Request $request)
    {
        $file = public_path(). "/usagerUploadExemple.xlsx";
        return \Response::download($file);
    }


    // to update usager informations
    public function updateUsager(Request $request){
        $validator = Validator::make($request->all(), [
            'usager_id' => 'required|exists:users,id',
            'name' => 'string|max:255',
            'prenom' => 'string|max:255',
            'email' => 'string|email|max:255|unique:users,email,'.$request->usager_id,
            'matricule' => 'nullable|max:5|unique:users,matricule,'.$request->usager_id,
            'codeBadge' => 'nullable|min:3|unique:users,codeBadge,'.$request->usager_id,
            'poste_id' => 'nullable|exists:postes,id',
            'type_id' => 'nullable|exists:types,id',
            'structure_id' => 'nullable|exists:structures,id',
            'categorie_id' => 'nullable|exists:categories,id',
            'typemenu_id' => 'nullable|array|min:1',
            'typemenu_id.*' => 'distinct|exists:type_menus,id',

        ],CustumValidatorMessages::message());

        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first());
        }

        return UserTools::updateUsager($request);
    }



    //  auto comptete Usager
    public function getUsagerAutocomplete(Request $request){
        try {
            $usager = User::select('id','name', 'matricule')
                ->where(function($query){
                    $query->where('profil', UserTools::USAGERPROFIL)
                        ->orWhereIn('codeRole',['AT','AF']);
                })
                ->where('status', true)
                ->where('name', 'LIKE', "%$request->search%")
                ->orderby('name')->get()
            ;
            return ApiResponseFormatTools::Format(true,'',$usager);

        }catch (\Exception $e){
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);
        }

    }


    // to get user Agent
    public function getAgent(Request $request){
        try {
            $limit = UtilsTools::limit($request);
            $page = UtilsTools::page($request);
            $skip = UtilsTools::skip($page,$limit);

            $liste = User::where('profil',UserTools::AGENTPROFIL);
            if (isset($request->name)){
                $liste = $liste->where('name', 'LIKE', "%$request->name%");
            }

            if (isset($request->role)){
                $liste = $liste->where('codeRole', $request->role);
            }

            if (isset($request->export) && $request->export=='EXCEL' ){
                return Excel::download(new AgentExport($liste->orderby('name')->get()), 'agent.xlsx');
            }

            $all = $liste->count();
            $liste = $liste->orderby('name')->with(['role','usertypemenu'])->limit($limit)->skip($skip)->get();

            $lastpage = UtilsTools::lastPage($all,$limit);
            return ApiResponseFormatTools::FormatPaginate(true,'',$all,$limit,$page, $lastpage, $liste);
        }catch (\Exception $e){
            return ApiResponseFormatTools::Format(false,$e->getMessage());

        }
    }


    // valide row to import
    private function validateRow($datas, $rowNumber) {
        $validator = Validator::make($datas, [
            'nom' => 'required|string|max:255',
            'prenom' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'matricule' => 'required|max:5|unique:users',
            'badge' => 'nullable|min:3|unique:users,codeBadge',
            'poste' => 'nullable|exists:postes,libelle',
            'type' => 'nullable|exists:types,libelle',
            'structure' => 'nullable|exists:structures,libelle',
            'categorie' => 'nullable|exists:categories,libelle',
            'service' => 'nullable|exists:services,libelle',
            'typemenu_id' => 'nullable|array|min:1',
            'typemenu_id.*' => 'nullable|distinct|exists:type_menus,libelle',
        ],CustumValidatorMessages::message());
        if ($validator->fails()){
            array_push($this->unvalidateRow,  array('ligne' => $rowNumber+1, "errors" => $validator->errors()->all() ));
            return false;
        }
        return true;
    }


}

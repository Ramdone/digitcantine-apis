<?php

namespace App\Http\Controllers;

use App\Models\Reservation;
use App\Tools\ApiResponseFormatTools;
use App\Tools\CustumValidatorMessages;
use App\Tools\ReservationTools;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ReservationController extends Controller
{
    // to make reservation
    public function makeReservation(Request $request){
        $validator = Validator::make($request->all(), [
            'menuRepas_id' => 'required|exists:menu_repas,id',
        ],CustumValidatorMessages::message());
        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first());
        }

        return ReservationTools::reservation($request);
    }

    // to get connected user reservation
    public function getMyReservation(Request $request){
        $validator = Validator::make($request->all(), [
            'dateDebut' => 'nullable|date_format:d-m-Y',
            'dateFin' => 'nullable|date_format:d-m-Y|after_or_equal:dateDebut',
        ]);
        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first());
        }
        return ReservationTools::myReservation($request);
    }


    // to get usager reservations
    public function getReservations(Request $request){
        $validator = Validator::make($request->all(), [
            'dateDebut' => 'nullable|date_format:d-m-Y',
            'dateFin' => 'nullable|date_format:d-m-Y|after_or_equal:dateDebut',
        ],CustumValidatorMessages::message());
        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first());
        }

        return ReservationTools::listReservation($request);

    }


    // to delete reservation
    public function deleteReservation(Request $request, $id){
        try {
            $item = Reservation::find($id);
            if (isset($item)){
                $item->delete();
                return ApiResponseFormatTools::Format(true,'Suppression avec succès');
            }
            return ApiResponseFormatTools::Format(false,'Non trouvé');
        } catch (\Exception $e) {
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);
        }

    }
}

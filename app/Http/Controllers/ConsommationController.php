<?php

namespace App\Http\Controllers;

use App\Exports\ConsommationExport;
use App\Models\Consommation;
use App\Tools\ApiResponseFormatTools;
use App\Tools\ConsommationTools;
use App\Tools\CustumValidatorMessages;
use App\Tools\UtilsTools;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class ConsommationController extends Controller
{
    // get usager who badge and verify all access
    public function performBadge(Request $request){
        $validator = Validator::make($request->all(), [
            'codeBadge' => 'required|min:3|exists:users,codeBadge',
            'myComptoir_id' => 'required|exists:ouverture_comptoirs,id',

        ],CustumValidatorMessages::message());
        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first());
        }

        return ConsommationTools::doBadge($request);
    }


    // to list of consommation to a comptoir in current
    public function getBadgeList(Request $request, $myComptoir){
        try {
            $limit = UtilsTools::limit($request);
            $page = UtilsTools::page($request);
            $skip = UtilsTools::skip($page,$limit);

            $myComptoir = is_numeric($myComptoir) ? $myComptoir : null ;

            $consomations = Consommation::where('ouverture_comptoir_id',$myComptoir)
                ->whereHas('usager', function ($query) use($request) {
                    if (isset($request->name)){
                        $query->where('name', 'LIKE', "%$request->name%");
                    }

                    if (isset($request->access_cantine)){
                        $query->where('access_cantine',$request->access_cantine);
                    }

                })
                ->orderby('created_at');

            if (isset($request->export) && $request->export=='EXCEL' ){
                return Excel::download(new ConsommationExport($consomations->get()), 'consommation.xlsx');
            }

            $all = $consomations->count();
            $consomations = $consomations->limit($limit)->skip($skip)->get();
            $lastpage = UtilsTools::lastPage($all,$limit);
            return ApiResponseFormatTools::FormatPaginate(true,'',$all,$limit,$page, $lastpage, $consomations);

        }catch (\Exception $e){
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);

        }

    }


    // to delete badge account
    public function deletebadge(Request $request, $id){
        try {
            $item = Consommation::find($id);
            if (isset($item)){
                $item->delete();
                return ApiResponseFormatTools::Format(true,'Suppression avec succès');
            }
            return ApiResponseFormatTools::Format(false,'Non trouvé');
        } catch (\Exception $e) {
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);
        }

    }


    // to get list of consommation
    public function getConsommationlist(Request $request){
        $validator = Validator::make($request->all(), [
        //    'date' => 'nullable|date',
        ],CustumValidatorMessages::message());

        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first());
        }
        return ConsommationTools::consommationList($request);
    }



    // to get current user consommation list
    public function getMyConsommationList(Request $request){
        $validator = Validator::make($request->all(), [
            'dateDebut' => 'nullable|date_format:d-m-Y',
            'dateFin' => 'nullable|date_format:d-m-Y|after_or_equal:dateDebut',
        ],CustumValidatorMessages::message());

        return ConsommationTools::myConsommationList($request);
    }
}

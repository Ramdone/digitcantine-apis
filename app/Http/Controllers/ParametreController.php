<?php

namespace App\Http\Controllers;

use App\Models\Parametre;
use App\Tools\ApiResponseFormatTools;
use Illuminate\Http\Request;

class ParametreController extends Controller
{
    // to store settings
    public function storeSettings(Request $request){
        try {
            $exist = Parametre::first();
            if (isset($exist)){
                $exist->update($request->all());
            }else{
                $exist = Parametre::create($request->all());
            }
            return ApiResponseFormatTools::Format(true,'',$exist);

        } catch (\Exception $e) {
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);
         }


    }
}

<?php

namespace App\Http\Controllers;

use App\Exports\InformationsExport;
use App\Tools\ApiResponseFormatTools;
use App\Tools\CrudTools;
use App\Tools\CustumValidatorMessages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class TypeController extends Controller
{
    // to create a Type
    public function createType(Request $request){
        $validator = Validator::make($request->all(), [
            'libelle' => 'required',
        ],CustumValidatorMessages::message());
        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first());
        }
        $model= app('App\Models\Type');
        return CrudTools::store($model, $request);
    }

    // to get Type list
    public function getType(Request $request){
        try {
            $liste = CrudTools::get(app('App\Models\Type'),$request);
            if (isset($request->export) && $request->export=='EXCEL' ){
                return Excel::download(new InformationsExport($liste), 'informations.xlsx');
            }
            return ApiResponseFormatTools::Format(true,'',$liste);
        } catch (\Exception $e) {
            return ApiResponseFormatTools::Format(false,$e->getMessage());
        }
    }

// to delete specifique Type
    public function deleteType(Request $request, $id){
        return CrudTools::delete(app('App\Models\Type'), $request, $id);
    }
}

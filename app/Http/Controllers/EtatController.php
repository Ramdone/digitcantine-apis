<?php

namespace App\Http\Controllers;

use App\Models\Consommation;
use App\Models\Parametre;
use App\Tools\ApiResponseFormatTools;
use App\Tools\CustumValidatorMessages;
use App\Tools\EtatTools;
use App\Tools\UtilsTools;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EtatController extends Controller
{
    // consommation by periode and categorie
    public function getConsommationByPeriode(Request $request){
        $validator = Validator::make($request->all(), [
            'dateDebut' => 'nullable|date_format:d-m-Y',
            'dateFin' => 'nullable|date_format:d-m-Y|after_or_equal:dateDebut',
        ],CustumValidatorMessages::message());
        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first());
        }
       return EtatTools::consommationByPeriode($request);

    }


    // consommation by month
    public function getConsommationByMonth(Request $request){
        return EtatTools::consommationByMonth($request);
    }


    // consommation by comptoir
    public function getConsommationByCompotoir(Request $request){
        $validator = Validator::make($request->all(), [
            'dateDebut' => 'nullable|date_format:d-m-Y',
            'dateFin' => 'nullable|date_format:d-m-Y|after_or_equal:dateDebut',
        ],CustumValidatorMessages::message());
        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first());
        }
        return EtatTools::consommationByCompotoir($request);

    }


    // consommation by year
    public function getConsommationByYear(Request $request){
        return EtatTools::consommationByYear($request);
    }
}

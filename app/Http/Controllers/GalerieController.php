<?php

namespace App\Http\Controllers;

use App\Models\Galerie;
use App\Models\Repas;
use App\Tools\ApiResponseFormatTools;
use App\Tools\CustumValidatorMessages;
use App\Tools\GalerieTools;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class GalerieController extends Controller
{
    // to add image to galerie
    public function addImage(Request $request){
        $validator = Validator::make($request->all(), [
            'photo' => 'required|mimes:jpg,jpeg,png|max:2048',
        ],CustumValidatorMessages::message());
        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first(),[],422);
        }

        return GalerieTools::uploadImageToGalerie($request);
    }


    // to get galerie
    public function getGalerie(Request $request){
        try {
            return ApiResponseFormatTools::Format(true,"",Galerie::where('status',true)->get());
        }catch (\Exception $e){
            return ApiResponseFormatTools::Format(false,$e->getMessage());
        }
    }


    // to delete images from galerie
    public function deleteImages(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required|array|min:1',
            'id.*' => 'required|exists:galeries,id',
        ],CustumValidatorMessages::message());
        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first());
        }

        try {
            $galerie = Galerie::whereIn('id',$request->id)->get();
            foreach ($galerie as $item){
                if (Repas::where('galerie_id',$item->id)->exists()){
                    $item->update(['status' => false]);
                }else{
                    File::delete(public_path($item->photo));
                    $item->delete();
                }
            }
            return ApiResponseFormatTools::Format(true,"");
        }catch (\Exception $e){
            return ApiResponseFormatTools::Format(false,$e->getMessage(),[],400);
        }

    }
}

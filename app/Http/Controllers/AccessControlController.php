<?php

namespace App\Http\Controllers;

use App\Exports\AccessControl;
use App\Exports\AgentExport;
use App\Models\DemandeControlAccess;
use App\Tools\AccessControlTools;
use App\Tools\ApiResponseFormatTools;
use App\Tools\ComptoirTools;
use App\Tools\CrudTools;
use App\Tools\CustumValidatorMessages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class AccessControlController extends Controller
{
    // to get pending access control
    public function getPendingAccessControl(Request $request){
        try {
            $pending = DemandeControlAccess::where('status', 0)->first();
            return ApiResponseFormatTools::Format(true,'',$pending);

        }catch (\Exception $e) {
            return ApiResponseFormatTools::Format(false,$e->getMessage());
        }

    }

    //to get current access control
    public function getCurrentAccessControl(Request $request){
        $data = array(
            'status' => ComptoirTools::getCantine()->verify_access,
            'enCour' => DemandeControlAccess::where('status', 0)->first()
        );
        return ApiResponseFormatTools::Format(true,'',$data );
    }


    // to ask for cancelling acces control
    public function askCancelControl(Request $request){
        $validator = Validator::make($request->all(), [
            'motif' => 'required',
        ],CustumValidatorMessages::message());
        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first(),[],200);
        }

        return AccessControlTools::askCancel($request);
    }


   // action access control
    public function actionOnAskCancel(Request $request){
        $validator = Validator::make($request->all(), [
            'status' => 'required|in:1,2',
            'motifRejet'=> 'required_if:status,==,2'
        ],CustumValidatorMessages::message());
        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first(),[],200);
        }

        if (! DemandeControlAccess::where('status', 0)->exists()){
            return ApiResponseFormatTools::Format(false,'Pas de demande en attente');
        }

        return AccessControlTools::actions($request);
    }


    // list of access control
    public function getAccesContols(Request $request){
        try {
            $data = [
                'pending' => DemandeControlAccess::where('status', 0)->first(),
                'all' => DemandeControlAccess::orderby('created_at')->get()
                ];

            if (isset($request->export) && $request->export=='EXCEL' ){
                return Excel::download(new AccessControl($data['all']), 'access_control.xlsx');
            }
            return ApiResponseFormatTools::Format(true,'',$data);

        } catch (\Exception $e) {
            return ApiResponseFormatTools::Format(false,$e->getMessage());
        }
    }


    // to allow acces control
    public function allowAccess(){
        try {
            $cantine = ComptoirTools::getCantine();
            $cantine->update(array('verify_access' => true));
            return ApiResponseFormatTools::Format(true,'',$cantine);
        } catch (\Exception $e) {
            return ApiResponseFormatTools::Format(false,$e->getMessage());
        }
    }

}

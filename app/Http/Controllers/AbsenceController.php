<?php

namespace App\Http\Controllers;

use App\Models\Absence;
use App\Tools\AbsenceTools;
use App\Tools\ApiResponseFormatTools;
use App\Tools\CrudTools;
use App\Tools\CustumValidatorMessages;
use App\Tools\UtilsTools;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AbsenceController extends Controller
{
    // to add Usager Absence
    public function createAbsence(Request $request){
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|exists:users,id',
            'motif' => 'required',
            'typeAbsence_id' => 'required|exists:type_absences,id',
            'dateDebut' => 'required|date_format:d-m-Y|after_or_equal:' . date('d-m-Y'),
            'dateFin' => 'required|date_format:d-m-Y|after_or_equal:dateDebut',
        ],CustumValidatorMessages::message());

        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first(),[],200);
        }
        $request['dateDebut'] = Carbon::parse($request->dateDebut)->startOfDay()->format(UtilsTools::projectDateFormat());
        $request['dateFin'] = Carbon::parse($request->dateFin)->endOfDay()->format(UtilsTools::projectDateFormat());
        return AbsenceTools::create($request);
    }


    // to get user absence
    public function getUserAbsences(Request $request, $id){
        try {
            $id = is_numeric($id) ? $id : null ;

            $absences = Absence::where('user_id',$id)->where('status',true)
                ->where('dateFin','>=',Carbon::now()->format(UtilsTools::projectDateFormat()))->get();
            //    ->where('dateDebut','>=',Carbon::now()->startOfDay())->get();
            return ApiResponseFormatTools::Format(true,'',$absences);

        }catch (\Exception $e){
            return ApiResponseFormatTools::Format(false,$e->getMessage());

        }
    }


    //  to update Absence
    public function updateAbsence(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required|exists:absences',
            'update_motif' => 'required',
            'typeAbsence_id' => 'exists:type_absences,id',
            'dateDebut' => 'date_format:d-m-Y',
            'dateFin' => 'date_format:d-m-Y|after:dateDebut',
        ],CustumValidatorMessages::message());
        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first());
        }
        $request['dateDebut'] = Carbon::parse($request->dateDebut)->startOfDay()->format(UtilsTools::projectDateFormat());
        $request['dateFin'] = Carbon::parse($request->dateFin)->endOfDay()->format(UtilsTools::projectDateFormat());
        return AbsenceTools::update($request);
    }


    // to delete specifique absence
    public function delete(Request $request, $id){
        return CrudTools::delete(app('App\Models\Absence'),$request, $id);
    }
}

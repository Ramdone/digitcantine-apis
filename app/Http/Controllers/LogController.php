<?php

namespace App\Http\Controllers;

use App\Models\UserTableLog;
use App\Tools\ApiResponseFormatTools;
use App\Tools\CustumValidatorMessages;
use App\Tools\UtilsTools;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LogController extends Controller
{
    // to get logs of modification on user table
    public function getLogs(Request $request){
        $validator = Validator::make($request->all(), [
            'dateDebut' => 'nullable|date_format:d-m-Y',
            'dateFin' => 'nullable|date_format:d-m-Y|after_or_equal:dateDebut',
        ],CustumValidatorMessages::message());
        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first());
        }

        $limit = UtilsTools::limit($request);
        $page = UtilsTools::page($request);
        $skip = UtilsTools::skip($page,$limit);

        $dateStart = isset($request->dateDebut) ? Carbon::parse($request->dateDebut)->startOfDay()->format(UtilsTools::projectDateFormat()) : Carbon::now()->startOfDay()->format(UtilsTools::projectDateFormat());
      //  $dateEnd = isset($request->dateFin) ? Carbon::parse($request->dateFin)->endOfDay()->format(UtilsTools::projectDateFormat()) : Carbon::now()->endOfDay()->format(UtilsTools::projectDateFormat());
        $dateEnd = isset($request->dateFin) ? Carbon::parse($request->dateFin)->endOfDay()->format(UtilsTools::projectDateFormat()) : null;

        if (isset($dateEnd)){
            $log = UserTableLog::whereBetween('created_at', [$dateStart,$dateEnd]);
        }else{
            $log = UserTableLog::whereDate('created_at','<=',$dateStart);
        }
        if (isset($request->name)){
            $log = $log->whereRelation('usager','name', 'LIKE', "%$request->name%")
                 ->orwhereRelation('usager','prenom', 'LIKE', "%$request->name%")
                 ->orwhereRelation('createur','name', 'LIKE', "%$request->name%")
                 ->orwhereRelation('createur','prenom', 'LIKE', "%$request->name%");

        }
        $all = $log->count();
        $lastpage = UtilsTools::lastPage($all,$limit);
        $log = $log->orderby('created_at','DESC')->limit($limit)->skip($skip)->get();

        return ApiResponseFormatTools::FormatPaginate(true,'',$all,$limit,$page, $lastpage, $log);






    }
}

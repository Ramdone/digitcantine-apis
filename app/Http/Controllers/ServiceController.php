<?php

namespace App\Http\Controllers;

use App\Exports\InformationsExport;
use App\Tools\ApiResponseFormatTools;
use App\Tools\CrudTools;
use App\Tools\CustumValidatorMessages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class ServiceController extends Controller
{
    // to create a service
    public function createService(Request $request){
        $validator = Validator::make($request->all(), [
            'libelle' => 'required',
        ],CustumValidatorMessages::message());
        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first());
        }
        $model= app('App\Models\Service');
        return CrudTools::store($model, $request);
    }

    // to get services list
    public function getService(Request $request){
        try {
            $liste = CrudTools::get(app('App\Models\Service'),$request);
            if (isset($request->export) && $request->export=='EXCEL' ){
                return Excel::download(new InformationsExport($liste), 'informations.xlsx');
            }
            return ApiResponseFormatTools::Format(true,'',$liste);
        } catch (\Exception $e) {
            return ApiResponseFormatTools::Format(false,$e->getMessage());
        }
    }

// to delete specifique service
    public function deleteService(Request $request,$id){
       return CrudTools::delete(app('App\Models\Service'),$request, $id);
    }
}

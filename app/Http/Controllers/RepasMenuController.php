<?php

namespace App\Http\Controllers;

use App\Exports\InformationsExport;
use App\Tools\ApiResponseFormatTools;
use App\Tools\CrudTools;
use App\Tools\CustumValidatorMessages;
use App\Tools\MenuRepasTools;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class RepasMenuController extends Controller
{
    // to create a Type Repas
    public function createTypeRepas(Request $request){
        $validator = Validator::make($request->all(), [
            'libelle' => 'required',
        ],CustumValidatorMessages::message());
        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages());
        }
        $model= app('App\Models\TypeRepas');
        return CrudTools::store($model, $request);
    }

    // to get Type list
    public function getTypeRepas(Request $request){
        try {
            $liste = CrudTools::get(app('App\Models\TypeRepas'),$request);
            if (isset($request->export) && $request->export=='EXCEL' ){
                return Excel::download(new InformationsExport($liste), 'informations.xlsx');
            }
            return ApiResponseFormatTools::Format(true,'',$liste);
        } catch (\Exception $e) {
            return ApiResponseFormatTools::Format(false,$e->getMessage());
        }
    }

// to delete specifique TypeRepas
    public function deleteTypeRepas(Request $request, $id){
        return CrudTools::delete(app('App\Models\TypeRepas'), $request, $id);
    }


    // to create a ELementRepas
    public function createElementRepas(Request $request){
        $validator = Validator::make($request->all(), [
            'libelle' => 'required',
        ],CustumValidatorMessages::message());
        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages());
        }
        $model= app('App\Models\ElementRepas');
        return CrudTools::store($model, $request);
    }

    // to get ElementRepas list
    public function getElementRepas(Request $request){
        try {
            $liste = CrudTools::get(app('App\Models\ElementRepas'),$request);
            if (isset($request->export) && $request->export=='EXCEL' ){
                return Excel::download(new InformationsExport($liste), 'informations.xlsx');
            }
            return ApiResponseFormatTools::Format(true,'',$liste);
        } catch (\Exception $e) {
            return ApiResponseFormatTools::Format(false,$e->getMessage());
        }
    }

    // to delete specifique ElementRepas
    public function deleteElementRepas(Request $request, $id){
        return CrudTools::delete(app('App\Models\ElementRepas'), $request, $id);
    }



    // to create Repas
    public function createRepas(Request $request){
        $validator = Validator::make($request->all(), [
            'libelle' => 'required|unique:repas',
            'typeRepas_id' => 'required|exists:type_repas,id',
            'galerie_id' => 'required|exists:galeries,id',
            'elementRepas_id' => 'required|array|min:1',
            "elementRepas_id.*"  => "required|distinct|exists:element_repas,id",

        ],CustumValidatorMessages::message());

        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first());
        }
        return MenuRepasTools::createRepas($request);
    }


    // get autocomplete repas
    public function repasAuto(Request $request){
        $model= app('App\Models\Repas');
        return CrudTools::autocomplete($model, $request);
    }


    // to create menu
    public function createMenu(Request $request){
        $validator = Validator::make($request->all(), [
            'date' => 'required|date_format:d-m-Y|after_or_equal:'.date('d-m-Y'),
            'repas' => 'required|array|min:1',
          //  "repas.*.repas_id"  => "required|distinct|exists:repas,id",
            "repas.*.repas_id"  => "required|exists:repas,id",
            "repas.*.type_menu_id"  => "required|exists:type_menus,id",
            "repas.*.reserv_is_consom"  => "required|boolean",
        ],CustumValidatorMessages::message());
        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first());
        }

        return MenuRepasTools::createMenu($request);

    }


    // to edit menu
    public function editMenu(Request $request){
        $validator = Validator::make($request->all(), [
            'datas' => 'required|array|min:1',
            'datas.*.menuRepas_id' => 'required|distinct|exists:menu_repas,id',
            'datas.*.repas_id' => 'required|distinct|exists:repas,id',
        ],CustumValidatorMessages::message());
        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first());
        }

        return MenuRepasTools::editMenu($request);

    }


    // to check if menu has reservation
    public function checkMenu(Request $request){
        $validator = Validator::make($request->all(), [
            'menuRepas_id' => 'required|array|min:1',
            'menuRepas_id.*' => 'required|distinct|exists:menu_repas,id',
        ],CustumValidatorMessages::message());
        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first());
        }
        return MenuRepasTools::checkMenu($request);
    }


    // delete menu
    public function deleteMenu(Request $request){
        $validator = Validator::make($request->all(), [
            'menuRepas_id' => 'required|array|min:1',
            'menuRepas_id.*' => 'required|distinct|exists:menu_repas,id',
        ],CustumValidatorMessages::message());
        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first());
        }

        return MenuRepasTools::deleteMenu($request);

    }



    // to get Menu
    public function getMenuOfDay(Request $request){
        $validator = Validator::make($request->all(), [
            'date' => 'nullable|date_format:d-m-Y',

        ],CustumValidatorMessages::message());
        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first());
        }
        return MenuRepasTools::menuOfDay($request);

    }


    // to get menu of a week
    public function getMenuOfWeek(Request $request){
        $validator = Validator::make($request->all(), [
            'week' => 'exists:menu_repas,id',

        ],CustumValidatorMessages::message());
        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first());
        }
        return MenuRepasTools::menuOfWeek($request);

    }


    // to send mail notif for menu of week
    public function sendMenuMailNotif(Request $request){
        $validator = Validator::make($request->all(), [
            'dateDebut' => 'required|date|after_or_equal:' . date('d-m-Y'),
            'dateFin' => 'date|after:dateDebut',

        ],CustumValidatorMessages::message());
        if ($validator->fails()) {
            return ApiResponseFormatTools::Format(false,$validator->messages()->first());
        }

        return MenuRepasTools::menuMail($request);
    }
}

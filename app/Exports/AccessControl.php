<?php

namespace App\Exports;

use App\Tools\UtilsTools;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AccessControl implements FromCollection, WithHeadings
{
    public $liste;

    public function __construct($liste)
    {
        $this->liste = $liste;
    }


    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $data =  array();
        foreach ($this->liste as $item){
            $data[]= array(
                "NOM ET PRENOMS DE L'AGENT" => isset($item->demandeur) ? $item->demandeur->name.' '.$item->demandeur->prenom : '',
                "DATE ET HEURE DE LA DEMANDE" => UtilsTools::datetimeFormat($item->created_at),
                "DATE ET HEURE DE LA VALIDATION" => UtilsTools::datetimeFormat($item->validated_at),
            );
        }
        return collect($data) ;
    }

    public function headings(): array
    {
        return [
            "NOM ET PRENOMS DE L'AGENT",
            "DATE ET HEURE DE LA DEMANDE",
            "DATE ET HEURE DE LA VALIDATION",
        ];
    }
}

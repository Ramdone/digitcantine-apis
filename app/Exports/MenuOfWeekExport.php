<?php

namespace App\Exports;

use App\Tools\UtilsTools;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class MenuOfWeekExport implements FromCollection, WithHeadings
{
    public $liste;

    public function __construct($liste)
    {
        $this->liste = $liste;
    }


    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $data =  array();
        foreach ($this->liste as $key=>$item){
            $data[]= array(
                "DATE" => UtilsTools::dateFormat($key),
                "" => isset($item[0]) ? $item[0]->repas->libelle :"",
                " " => isset($item[1]) ? $item[1]->repas->libelle :"",
                "  " => isset($item[2]) ? $item[2]->repas->libelle :"",
                "   " => isset($item[3]) ? $item[3]->repas->libelle :"",
            );
        }
        return collect($data) ;
    }

    public function headings(): array
    {
        return [
            "DATE",
            "Repas 1",
            "Repas 2",
            "Repas 3",
            "Repas 4",
        ];
    }
}

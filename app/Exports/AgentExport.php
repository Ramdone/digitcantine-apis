<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AgentExport implements FromCollection, WithHeadings
{
    public $liste;

    public function __construct($liste)
    {
        $this->liste = $liste;
    }


    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $data =  array();
        foreach ($this->liste as $item){
            $data[]= array(
                "NOM ET PRENOMS" => $item->name.' '.$item->prenom,
                "EMAIL" => $item->email,
                "PROFIL" => isset($item->role) ? $item->role->libelle : '',
            );
        }
        return collect($data) ;
    }

    public function headings(): array
    {
        return [
            "NOM ET PRENOMS",
            "EMAIL",
            "PROFIL",
        ];
    }
}

<?php

namespace App\Exports;

use App\Tools\UtilsTools;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReservationExport implements FromCollection, WithHeadings
{
    public $liste;

    public function __construct($liste)
    {
        $this->liste = $liste;
    }


    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $data =  array();
        foreach ($this->liste as $item){
            $data[]= array(
                "NOM ET PRENOMS" => $item->usager->name.' '.$item->usager->prenom,
                "STRUCTURE" => isset($item->usager->structure) ? $item->usager->structure->libelle : '',
                "RÉSERVATIONS" =>  $item->menuRepas->repas->libelle,
                "TYPE DE MENU" => isset($item->menuRepas->typemenu) ? $item->menuRepas->typemenu->libelle : '',
                "Date" => UtilsTools::dateFormat($item->dateReservedFor),
            );
        }
        return collect($data) ;
    }

    public function headings(): array
    {
        return [
            "NOM ET PRENOMS",
            "STRUCTURE",
            "RÉSERVATIONS",
            "TYPE DE MENU",
            "Date",
        ];
    }
}

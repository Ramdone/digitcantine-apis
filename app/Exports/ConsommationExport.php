<?php

namespace App\Exports;

use App\Tools\UtilsTools;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ConsommationExport implements FromCollection, WithHeadings
{
    public $liste;

    public function __construct($liste)
    {
        $this->liste = $liste;
    }


    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $data =  array();
        foreach ($this->liste as $item){
            $data[]= array(
                "NOM ET PRENOMS" => $item->usager->name.' '.$item->usager->prenom,
                "STRUCTURE" => isset($item->usager->structure) ? $item->usager->structure->libelle:'',
                "RÉSERVATIONS" => isset($item->reservation) ? $item->reservation->menuRepas->repas->libelle : '',
                "CONSOMMATIONS" => $item->ouvertureComptoir->menuRepas->repas->libelle,
                "NBRE DE REPAS / J" => $item->nbr_repas,
                "NBRE DE REPAS CONSOMME" => $item->nbr_consommation,
                "DATE ET HEURE CONSOMMATION" => UtilsTools::datetimeFormat($item->created_at),
                "L'OPERATEUR DE COMPTOIR" => $item->ouvertureComptoir->agent->name.' '.$item->ouvertureComptoir->agent->prenom,
            );
        }
        return collect($data) ;
    }

    public function headings(): array
    {
        return [
            "NOM ET PRENOMS",
            "STRUCTURE",
            "RÉSERVATIONS",
            "CONSOMMATIONS",
            "NBRE DE REPAS / J",
            "NBRE DE REPAS CONSOMME",
            "DATE ET HEURE CONSOMMATION",
            "L'OPERATEUR DE COMPTOIR",
        ];
    }
}

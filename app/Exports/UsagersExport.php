<?php

namespace App\Exports;

use App\Tools\UtilsTools;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;


class UsagersExport implements FromCollection, WithHeadings
{
    public $liste;

    public function __construct($liste)
    {
        $this->liste = $liste;
    }


    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $data =  array();
        foreach ($this->liste as $item){
            $data[]= array(
                "MATRICULE" => $item->matricule,
                "NOM ET PRENOMS" => $item->name.' '.$item->prenom,
                "SERVICE" => isset($item->service) ? $item->service->libelle : '',
                "FONCTION" => isset($item->poste) ? $item->poste->libelle : '',
                "CATEGORIES SOCIO-PROFESSIONNELLE" =>  isset($item->categorie) ? $item->categorie->libelle : '' ,
                "OBSERVATION" => isset($item->Absence) ? $item->Absence->motif : '' ,
                "PERIODE ABSENCE" => isset($item->Absence) ? UtilsTools::dateFormat($item->Absence->dateDebut).' - '.UtilsTools::dateFormat($item->Absence->dateFin) : '' ,
                "STRUCTURE" => isset($item->structure) ? $item->structure->libelle : '',
                "NBRE DE REPAS / J" => $item->nbr_repas ,
            );
        }
        return collect($data) ;
    }

    public function headings(): array
    {
        return [
            "MATRICULE",
            "NOM ET PRENOMS",
            "SERVICE",
            "FONCTION",
            "CATEGORIES SOCIO-PROFESSIONNELLE",
            "OBSERVATION",
            "PERIODE ABSENCE",
            "STRUCTURE",
            "NBRE DE REPAS / J"
        ];
    }
}

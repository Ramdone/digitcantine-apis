<?php

namespace App\Exports;

use App\Tools\UtilsTools;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class OpenComptoirExport implements FromCollection, WithHeadings
{
    public $liste;

    public function __construct($liste)
    {
        $this->liste = $liste;
    }


    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $data =  array();
        foreach ($this->liste as $item){
            $data[]= array(
                "DATE ET HEURE D'OUVERTURE" => UtilsTools::datetimeFormat($item->dateDebut),
                "DATE ET HEURE DE FERMETURE" => UtilsTools::datetimeFormat($item->dateFin),
                "RESTAURATEUR" => $item->agent->name.' '.$item->agent->prenom,
                "COMPTOIR" =>  $item->comptoir->libelle,
                "REPAS" =>  $item->menuRepas->repas->libelle,

            );
        }
        return collect($data) ;
    }

    public function headings(): array
    {
        return [
            "DATE ET HEURE D'OUVERTURE",
            "DATE ET HEURE DE FERMETURE",
            "RESTAURATEUR",
            "COMPTOIR",
            "REPAS",
        ];
    }
}

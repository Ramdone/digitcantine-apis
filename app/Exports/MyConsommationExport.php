<?php

namespace App\Exports;

use App\Tools\UtilsTools;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class MyConsommationExport implements FromCollection, WithHeadings
{
    public $liste;

    public function __construct($liste)
    {
        $this->liste = $liste;
    }


    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $data =  array();
        foreach ($this->liste as $item){
            $data[]= array(
                "DATE" => UtilsTools::dateFormat($item->created_at),
                "COMPTOIR" => $item->ouvertureComptoir->comptoir->libelle,
                "REPAS RÉSERVÉ" => isset($item->reservation) ? $item->reservation->menuRepas->repas->libelle : '',
                "REPAS CONSOMMÉ" => $item->ouvertureComptoir->menuRepas->repas->libelle,
            );
        }
        return collect($data) ;
    }

    public function headings(): array
    {
        return [
            "DATE",
            "COMPTOIR",
            "REPAS RÉSERVÉ",
            "REPAS CONSOMMÉ",
        ];
    }
}
